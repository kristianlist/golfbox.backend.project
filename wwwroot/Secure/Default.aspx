﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Secure_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="Server">
    <div class="blue-bar">
        <div class="menu" id="view-menu">
            <div class="legend"></div>
            <div data-view="rules" class="item"><span>Rules Control</span></div>
            <div data-view="issues" class="item"><span>Current Issues</span></div>
            <div data-view="users" class="item"><span>User Status</span></div>
            <div data-view="update" class="item"><span>Update</span></div>
            <div data-view="gantt" class="item"><span>Gantt</span></div>
        </div>
    </div>
    <div class="spacer"></div>
    <div id="view-holder">
        <div id="view-rules" class="hide">
            <div id="view-rules-current"></div>
            <div style="height:30px;"></div>
            <div id="view-rules-candidates"></div>
            <div style="height:30px;"></div>
            <div id="view-rules-backlog"></div>
        </div>
        <div id="view-users" class="hide"></div>
        <div id="view-issues" class="hide">
            <div id="view-issues-red"></div>
            <div style="height:30px;"></div>
            <div id="view-issues-yellow"></div>
        </div>
        <div id="view-gantt">
            <div id="gantt-container"></div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="Server">
    <script src="/Scripts/Pages/secure_default.js"></script>
</asp:Content>
