namespace WebSite
{
    using System.Web;
    using System.Collections.Generic;
    using System.Linq;

    public class Helper
    {
        internal static void WriteJsonToClient(HttpContext context, object obj)
        {
            HttpRequest req = context.Request;
            HttpResponse res = context.Response;
            string json = Converter.ToJson(obj, Converter.ToBool(req["jsonIndented"]));

            if (Converter.IsEmpty(req["callback"]))
            {
                res.ContentType = "application/json";
                res.Write(json);
            }
            else
            {
                res.ContentType = "application/x-javascript";
                res.Write(req["callback"] + "(" + json + ")");
            }
        }
    }
}