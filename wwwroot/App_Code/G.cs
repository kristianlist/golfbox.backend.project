﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Translate
/// </summary>
public static class G
{
    public static string GenerateRandomString(int length)
    {
        Random r = new Random(Environment.TickCount);
        string chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        System.Text.StringBuilder sb = new System.Text.StringBuilder(length);
        for (int i = 0; i < length; ++i)
            sb.Append(chars[r.Next(chars.Length)]);
        return sb.ToString();
    }
}