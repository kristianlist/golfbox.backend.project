﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using GolfBox.Framework.Data;
using GolfBox.RM.Entities.Project;

namespace GolfBox.RM.Data.Project
{
    internal class ProjectDalc
    {
        /// <summary>
        /// This Method sets the corrosponding values from the database in the ProjectData object.
        /// </summary>
        /// <author>Sune 'The King' Olsen</author>
        /// <created>18-12-2015</created>
        internal static void SetValues(SqlDataReader reader, ref ProjectData data)
        {
            if (reader.HasRows)
            {
                data.ID = SqlReader.Read<int>(reader, "Project_ID");
                data.Name = SqlReader.Read<string>(reader, "Project_Name");
                data.Identifier = SqlReader.Read<string>(reader, "Project_Identifier");
                data.Description = SqlReader.Read<string>(reader, "Project_Description");                
                data.Parent_ID = SqlReader.Read<int>(reader, "Project_Parent_ID");
                data.CreatedOn = SqlReader.Read<DateTime>(reader, "Project_CreatedOn");
                data.UpdatedOn = SqlReader.ReadNullableValue<DateTime>(reader, "Project_UpdatedOn");
            }
        }

        /// <summary>
        /// This Method constructs the ProjectData object.
        /// </summary>
        /// <author>Sune 'The King' Olsen</author>
        /// <created>18-12-2015</created>
        internal static ProjectData CreateItem(SqlDataReader reader)
        {
            ProjectData _outdata = new ProjectData();
            SetValues(reader, ref _outdata);
            return _outdata;
        }

        /// <summary>
        /// This Method inserts a ProjectData object and returns the inserted object
        /// </summary>
        /// <author>Sune 'The King' Olsen</author>
        /// <created>18-12-2015</created>
        internal static ProjectData Insert(ProjectData indata)
        {
            return new Listor("[SSP.Redmine.Projects.Insert]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Project_ID", indata.ID),
                    new Listor.Send<int?>("@Project_Parent_ID", indata.Parent_ID),
                    new Listor.Send<string>("@Project_Name", indata.Name),
                    new Listor.Send<string>("@Project_Identifier", indata.Identifier),
                    new Listor.Send<string>("@Project_Description", indata.Description),
                    new Listor.Send<DateTime>("@Project_CreatedOn", indata.CreatedOn),
                    new Listor.SendNothing<DateTime?>("@Project_UpdatedOn", indata.UpdatedOn),
                }
            }.GetInstance<ProjectData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method updates a ProjectData object and returns the updated object
        /// </summary>
        /// <author>Sune 'The King' Olsen</author>
        /// <created>18-12-2015</created>
        internal static ProjectData Update(ProjectData indata)
        {
            return new Listor("[SSP.Redmine.Projects.Update]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Project_ID", indata.ID),
                    new Listor.Send<int?>("@Project_Parent_ID", indata.Parent_ID),
                    new Listor.Send<string>("@Project_Name", indata.Name),
                    new Listor.Send<string>("@Project_Identifier", indata.Identifier),
                    new Listor.Send<string>("@Project_Description", indata.Description),
                    new Listor.Send<DateTime>("@Project_CreatedOn", indata.CreatedOn),
                    new Listor.SendNothing<DateTime?>("@Project_UpdatedOn", indata.UpdatedOn),
                }
            }.GetInstance<ProjectData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method loads a ProjectData object and returns the loaded object
        /// </summary>
        /// <author>Sune 'The King' Olsen</author>
        /// <created>06-01-2015</created>
        internal static ProjectData Load(int productID)
        {
            return new Listor("[SSP.Redmine.Projects.Load]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Product_ID", productID)
                }
            }.GetInstance<ProjectData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method deletes a ProjectData object and returns the deleted object
        /// </summary>
        /// <author>Sune 'The King' Olsen</author>
        /// <created>06-01-2015</created>
        internal static ProjectData Delete(int productID)
        {
            return new Listor("[SSP.Redmine.Projects.Delete]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Product_ID", productID)
                }
            }.GetInstance<ProjectData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        internal static class Lists
        {
            /// <summary>
            /// This Method gets a list of ProjectData
            /// </summary>
            /// <author>Sune 'The King' Olsen</author>
            /// <created>06-01-2015</created>
            internal static List<ProjectData> All()
            {
                return new Listor("[SSP.Redmine.Projects.Lists.All]").GetList<ProjectData>(delegate(SqlDataReader reader)
                {
                    return CreateItem(reader);
                });
            }
        }
    }
}