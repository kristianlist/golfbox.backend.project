﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using GolfBox.Framework.Data;
using GolfBox.RM.Entities.TimeEntry;


namespace GolfBox.RM.Data.TimeEntry
{
    internal class TimeEntryDalc
    {
        /// <summary>
        /// This Method sets the corrosponding values from the database in the TimeEntryData object.
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static void SetValues(SqlDataReader reader, bool minimizeLoad, ref TimeEntryData data)
        {
            if (reader.HasRows)
            {
                data.ID = SqlReader.Read<int>(reader, "TimeEntry_ID");
                data.Hours = SqlReader.Read<decimal>(reader, "TimeEntry_Hours");
                data.IssueID = SqlReader.Read<int>(reader, "Issue_ID");

                if (!minimizeLoad) {
                    data.SpentOn = SqlReader.Read<DateTime>(reader, "TimeEntry_SpentOn");
                    data.Comment = SqlReader.Read<string>(reader, "TimeEntry_Comment");
                    data.ProjectID = SqlReader.Read<int>(reader, "Project_ID");
                    data.UserID = SqlReader.Read<int>(reader, "User_ID");
                    data.ActivityID = SqlReader.Read<int>(reader, "Activity_Id");
                    data.CreatedOn = SqlReader.Read<DateTime>(reader, "TimeEntry_CreatedOn");
                    data.UpdatedOn = SqlReader.ReadNullableValue<DateTime>(reader, "TimeEntry_UpdatedOn");
                }
            }
        }

        /// <summary>
        /// This Method constructs the TimeEntryData object.
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static TimeEntryData CreateItem(SqlDataReader reader, bool minimizeLoad = false)
        {
            TimeEntryData _outdata = new TimeEntryData();
            SetValues(reader, minimizeLoad, ref _outdata);
            return _outdata;
        }

        /// <summary>
        /// This Method inserts or updates a TimeEntryData object 
        /// </summary>
        /// <author>Peter Frost</author>
        /// <created>06-09-2016</created>
        internal static TimeEntryData Save(TimeEntryData indata)
        {
            return new Listor("[SSP.Redmine.TimeEntries.Save]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@TimeEntry_ID", indata.ID),
                    new Listor.Send<decimal>("@TimeEntry_Hours", indata.Hours),
                    new Listor.Send<DateTime>("@TimeEntry_SpentOn", indata.SpentOn),
                    new Listor.SendNothing<string>("@TimeEntry_Comment", indata.Comment),
                    new Listor.Send<int>("@Project_ID", indata.ProjectID),
                    new Listor.Send<int>("@Issue_ID", indata.IssueID),
                    new Listor.Send<int>("@User_ID", indata.UserID),
                    new Listor.Send<int>("@Activity_Id", indata.ActivityID),
                    new Listor.Send<DateTime>("@TimeEntry_CreatedOn", indata.CreatedOn),
                    new Listor.SendNothing<DateTime?>("@TimeEntry_UpdatedOn", indata.UpdatedOn),
                }
            }.GetInstance<TimeEntryData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method inserts a TimeEntryData object and returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static TimeEntryData Insert(TimeEntryData indata)
        {
            return new Listor("[SSP.Redmine.TimeEntries.Insert]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@TimeEntry_ID", indata.ID),
                    new Listor.Send<decimal>("@TimeEntry_Hours", indata.Hours),
                    new Listor.Send<DateTime>("@TimeEntry_SpentOn", indata.SpentOn),
                    new Listor.SendNothing<string>("@TimeEntry_Comment", indata.Comment),
                    new Listor.Send<int>("@Project_ID", indata.ProjectID),
                    new Listor.Send<int>("@Issue_ID", indata.IssueID),
                    new Listor.Send<int>("@User_ID", indata.UserID),
                    new Listor.Send<int>("@Activity_Id", indata.ActivityID),
                    new Listor.Send<DateTime>("@TimeEntry_CreatedOn", indata.CreatedOn),
                    new Listor.SendNothing<DateTime?>("@TimeEntry_UpdatedOn", indata.UpdatedOn),
                }
            }.GetInstance<TimeEntryData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method updates a TimeEntryData object and returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static TimeEntryData Update(TimeEntryData indata)
        {
            return new Listor("[SSP.Redmine.TimeEntries.Update]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@TimeEntry_ID", indata.ID),
                    new Listor.Send<decimal>("@TimeEntry_Hours", indata.Hours),
                    new Listor.Send<DateTime>("@TimeEntry_SpentOn", indata.SpentOn),
                    new Listor.SendNothing<string>("@TimeEntry_Comment", indata.Comment),
                    new Listor.Send<int>("@Project_ID", indata.ProjectID),
                    new Listor.Send<int>("@Issue_ID", indata.IssueID),
                    new Listor.Send<int>("@User_ID", indata.UserID),
                    new Listor.Send<int>("@Activity_Id", indata.ActivityID),
                    new Listor.Send<DateTime>("@TimeEntry_CreatedOn", indata.CreatedOn),
                    new Listor.SendNothing<DateTime?>("@TimeEntry_UpdatedOn", indata.UpdatedOn),
                }
            }.GetInstance<TimeEntryData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method loads a TimeEntryData object and returns the loaded object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static TimeEntryData Load(int timeEntryID)
        {
            return new Listor("[SSP.Redmine.TimeEntries.Load]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@TimeEntry_ID", timeEntryID)
                }
            }.GetInstance<TimeEntryData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method deletes a TimeEntryData object and returns the deleted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static TimeEntryData Delete(int productID)
        {
            return new Listor("[SSP.Redmine.TimeEntries.Delete]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Product_ID", productID)
                }
            }.GetInstance<TimeEntryData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        internal static class Lists
        {
            /// <summary>
            /// This Method gets a list of TimeEntryData
            /// </summary>
            /// <author>Sune Bruun Olsen</author>
            /// <created>18-12-2015</created>
            internal static List<TimeEntryData> All()
            {
                return new Listor("[SSP.Redmine.TimeEntries.Lists.All]").GetList<TimeEntryData>(delegate(SqlDataReader reader)
                {
                    return CreateItem(reader);
                });
            }

            internal static List<TimeEntryData> ByIssueList(List<int> issueIdList, bool minimizeLoad = false)
            {
                if (issueIdList.Count > 0)
                {
                    return new Listor("[SSP.Redmine.TimeEntries.Lists.ByIssueList]")
                    {
                        Params = new Listor.ISender[]{
                            new Listor.Sender("@IssueIDList", string.Join(",", issueIdList))
                        }
                    }.GetList<TimeEntryData>(delegate(SqlDataReader reader)
                    {
                        return CreateItem(reader, minimizeLoad);
                    });
                }

                return new List<TimeEntryData>();
            }

            internal static List<TimeEntryData> ByVersionAndOpen(int versionID, bool minimizeLoad) {
                return new Listor("[SSP.Redmine.TimeEntries.Lists.ByVersionAndOpen]") {
                    Params = new Listor.ISender[]{
                        new Listor.Sender("@Version_ID",versionID)
                    }
                }.GetList<TimeEntryData>(delegate (SqlDataReader reader) {
                    return CreateItem(reader, minimizeLoad);
                });
            }

            internal static List<TimeEntryData> ByUserList(List<int> userIdList)
            {
                if (userIdList.Count > 0)
                {
                    return new Listor("[SSP.Redmine.TimeEntries.Lists.ByUserList]")
                    {
                        Params = new Listor.ISender[]{
                            new Listor.Sender("@UserIDList", string.Join(",", userIdList))
                        }
                    }.GetList<TimeEntryData>(delegate(SqlDataReader reader)
                    {
                        return CreateItem(reader);
                    });
                }

                return new List<TimeEntryData>();
            }
        }

    
    }
}

