﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GolfBox.RM.Data
{

    public class ConnectionStringConfig
    {
        public static string ConnectionString
        {
            get { return "GolfBox.Backend.Project.ConnectionString"; }
        }
    }

    public class Listor : GolfBox.Framework.Data.Listor
    {
        public Listor(string names)
            : base(names, ConnectionStringConfig.ConnectionString)
        {

        }

        public new string ConnectionString
        {
            get { return base.ConnectionString; }
        }
    }

}
