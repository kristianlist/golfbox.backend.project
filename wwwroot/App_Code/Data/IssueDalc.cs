﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using GolfBox.Framework.Data;
using GolfBox.RM.Entities.Issue;
using System.Threading.Tasks;

namespace GolfBox.RM.Data.Issue {
    internal class IssueDalc {
        /// <summary>
        /// This Method sets the corrosponding values from the database in the IssueData object.
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static void SetValues(SqlDataReader reader, bool minimizeLoad, ref IssueData data) {
            if (reader.HasRows) {
                data.ID = SqlReader.Read<int>(reader, "Issue_ID");
                data.Subject = SqlReader.Read<string>(reader, "Issue_Subject");
                data.EstimatedHours = SqlReader.Read<decimal>(reader, "Issue_EstimatedHours");
                data.EstimatedRisk = SqlReader.Read<int>(reader, "Issue_EstimatedRisk");
                data.DoneRatio = SqlReader.Read<int>(reader, "Issue_DoneRatio");
                data.StatusID = SqlReader.Read<int>(reader, "Status_ID");
                data.ProjectID = SqlReader.Read<int>(reader, "Project_ID");
                data.AssignedToID = SqlReader.Read<int>(reader, "AssignedTo_ID");

                if (!minimizeLoad) {
                    data.ParentID = SqlReader.ReadNullableValue<int>(reader, "Issue_Parent_ID");
                    data.TrackerID = SqlReader.Read<int>(reader, "Tracker_ID");
                    data.PriorityID = SqlReader.Read<int>(reader, "Priority_ID");
                    data.AuthorID = SqlReader.Read<int>(reader, "Author_ID");
                    data.FixedVersionID = SqlReader.Read<int>(reader, "FixedVersion_ID");
                    data.DueDate = SqlReader.ReadNullableValue<DateTime>(reader, "Issue_DueDate");
                    data.CreatedOn = SqlReader.Read<DateTime>(reader, "Issue_CreatedOn");
                    data.UpdatedOn = SqlReader.ReadNullableValue<DateTime>(reader, "Issue_UpdatedOn");
                    data.Description = SqlReader.Read<string>(reader, "Issue_Description");
                    data.NextGroomingDate = SqlReader.ReadNullableValue<DateTime>(reader, "Issue_NextGroomingDate");
                }
            }
        }

        /// <summary>
        /// This Method constructs the IssueData object.
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static IssueData CreateItem(SqlDataReader reader, bool minimizeLoad = false) {
            IssueData _outdata = new IssueData();
            SetValues(reader, minimizeLoad, ref _outdata);
            return _outdata;
        }

        /// <summary>
        /// This Method inserts or updates a IssueData object 
        /// </summary>
        /// <author>Peter Frost</author>
        /// <created>10-02-2016</created>
        internal static IssueData Save(IssueData indata) {
            return new Listor("[SSP.Redmine.Issues.Save]") {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Issue_ID", indata.ID),
                    new Listor.SendNothing<int?>("@Issue_Parent_ID", indata.ParentID),
                    new Listor.Send<string>("@Issue_Subject", indata.Subject),
                    new Listor.Send<string>("@Issue_Description", indata.Description),
                    new Listor.SendNothing<decimal?>("@Issue_EstimatedHours", indata.EstimatedHours),
                    new Listor.SendNothing<int?>("@Issue_EstimatedRisk", indata.EstimatedRisk),
                    new Listor.Send<int>("@Issue_DoneRatio", indata.DoneRatio),
                    new Listor.Send<int>("@Status_ID", indata.StatusID),
                    new Listor.Send<int>("@Project_ID", indata.ProjectID),
                    new Listor.Send<int>("@Tracker_ID", indata.TrackerID),
                    new Listor.Send<int>("@Priority_ID", indata.PriorityID),
                    new Listor.Send<int>("@Author_ID", indata.AuthorID),
                    new Listor.SendNothing<int?>("@AssignedTo_ID", indata.AssignedToID),
                    new Listor.Send<int>("@FixedVersion_ID", indata.FixedVersionID),
                    new Listor.SendNothing<DateTime?>("@Issue_DueDate", indata.DueDate),
                    new Listor.SendNothing<DateTime?>("@Issue_NextGroomingDate", indata.NextGroomingDate),
                    new Listor.Send<DateTime>("@Issue_CreatedOn", indata.CreatedOn),
                    new Listor.SendNothing<DateTime?>("@Issue_UpdatedOn", indata.UpdatedOn),
                }
            }.GetInstance<IssueData>(delegate (SqlDataReader reader) {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method inserts a IssueData object and returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static IssueData Insert(IssueData indata) {
            return new Listor("[SSP.Redmine.Issues.Insert]") {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Issue_ID", indata.ID),
                    new Listor.SendNothing<int?>("@Issue_Parent_ID", indata.ParentID),
                    new Listor.Send<string>("@Issue_Subject", indata.Subject),
                    new Listor.Send<string>("@Issue_Description", indata.Description),
                    new Listor.SendNothing<decimal?>("@Issue_EstimatedHours", indata.EstimatedHours),
                    new Listor.SendNothing<int?>("@Issue_EstimatedRisk", indata.EstimatedRisk),
                    new Listor.Send<int>("@Issue_DoneRatio", indata.DoneRatio),
                    new Listor.Send<int>("@Status_ID", indata.StatusID),
                    new Listor.Send<int>("@Project_ID", indata.ProjectID),
                    new Listor.Send<int>("@Tracker_ID", indata.TrackerID),
                    new Listor.Send<int>("@Priority_ID", indata.PriorityID),
                    new Listor.Send<int>("@Author_ID", indata.AuthorID),
                    new Listor.SendNothing<int?>("@AssignedTo_ID", indata.AssignedToID),
                    new Listor.Send<int>("@FixedVersion_ID", indata.FixedVersionID),
                    new Listor.SendNothing<DateTime?>("@Issue_DueDate", indata.DueDate),
                    new Listor.SendNothing<DateTime?>("@Issue_NextGroomingDate", indata.NextGroomingDate),
                    new Listor.Send<DateTime>("@Issue_CreatedOn", indata.CreatedOn),
                    new Listor.SendNothing<DateTime?>("@Issue_UpdatedOn", indata.UpdatedOn),
                }
            }.GetInstance<IssueData>(delegate (SqlDataReader reader) {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method updates a IssueData object and returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static IssueData Update(IssueData indata) {
            return new Listor("[SSP.Redmine.Issues.Update]") {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Issue_ID", indata.ID),
                    new Listor.SendNothing<int?>("@Issue_Parent_ID", indata.ParentID),
                    new Listor.Send<string>("@Issue_Subject", indata.Subject),
                    new Listor.Send<string>("@Issue_Description", indata.Description),
                    new Listor.SendNothing<decimal?>("@Issue_EstimatedHours", indata.EstimatedHours),
                    new Listor.SendNothing<int?>("@Issue_EstimatedRisk", indata.EstimatedRisk),
                    new Listor.Send<int>("@Issue_DoneRatio", indata.DoneRatio),
                    new Listor.Send<int>("@Status_ID", indata.StatusID),
                    new Listor.Send<int>("@Project_ID", indata.ProjectID),
                    new Listor.Send<int>("@Tracker_ID", indata.TrackerID),
                    new Listor.Send<int>("@Priority_ID", indata.PriorityID),
                    new Listor.Send<int>("@Author_ID", indata.AuthorID),
                    new Listor.SendNothing<int?>("@AssignedTo_ID", indata.AssignedToID),
                    new Listor.Send<int>("@FixedVersion_ID", indata.FixedVersionID),
                    new Listor.SendNothing<DateTime?>("@Issue_DueDate", indata.DueDate),
                    new Listor.SendNothing<DateTime?>("@Issue_NextGroomingDate", indata.NextGroomingDate),
                    new Listor.Send<DateTime>("@Issue_CreatedOn", indata.CreatedOn),
                    new Listor.SendNothing<DateTime?>("@Issue_UpdatedOn", indata.UpdatedOn),
                }
            }.GetInstance<IssueData>(delegate (SqlDataReader reader) {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method loads a IssueData object and returns the loaded object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static IssueData Load(int issueID) {
            return new Listor("[SSP.Redmine.Issues.Load]") {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Issue_ID", issueID)
                }
            }.GetInstance<IssueData>(delegate (SqlDataReader reader) {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method deletes a IssueData object and returns the deleted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static IssueData Delete(int issueID) {
            return new Listor("[SSP.Redmine.Issues.Delete]") {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@Issue_ID", issueID)
                }
            }.GetInstance<IssueData>(delegate (SqlDataReader reader) {
                return CreateItem(reader);
            });
        }

        internal static class Lists {
            /// <summary>
            /// This Method gets a list of IssueData
            /// </summary>
            /// <author>Sune Bruun Olsen</author>
            /// <created>18-12-2015</created>
            internal static List<IssueData> All() {
                return new Listor("[SSP.Redmine.Issues.Lists.All]").GetList<IssueData>(delegate (SqlDataReader reader) {
                    return CreateItem(reader);
                });
            }

            /// <summary>
            /// This Method gets a list of IssueData by Version
            /// </summary>
            /// <author>Peter Frost</author>
            /// <created>06-09-2016</created>
            internal static List<IssueData> ByVersion(int versionID, bool minimizeLoad) {
                return new Listor("[SSP.Redmine.Issues.Lists.ByVersion]") {
                    Params = new Listor.ISender[]{
                        new Listor.Sender("@Version_ID",versionID)
                    }
                }.GetList<IssueData>(delegate (SqlDataReader reader) {
                    return CreateItem(reader, minimizeLoad);
                });
            }

            /// <summary>
            /// This Method gets a list of IssueData by Version
            /// </summary>
            /// <author>Kristian List</author>
            /// <created>13-05-2020</created>
            internal static List<IssueData> ByVersionAndOpen(int versionID, bool minimizeLoad) {
                return new Listor("[SSP.Redmine.Issues.Lists.ByVersionAndOpen]") {
                    Params = new Listor.ISender[]{
                        new Listor.Sender("@Version_ID",versionID)
                    }
                }.GetList<IssueData>(delegate (SqlDataReader reader) {
                    return CreateItem(reader, minimizeLoad);
                });
            }

            /// <summary>
            /// This Method gets a list of IssueData by Assignee
            /// </summary>
            /// <author>Peter Frost</author>
            /// <created>09-09-2016</created>
            internal static List<IssueData> ByAssigneeList(List<int> userIdList) {
                if (userIdList.Count > 0) {
                    return new Listor("[SSP.Redmine.Issues.Lists.ByAssignee]") {
                        Params = new Listor.ISender[]{
                            new Listor.Sender("@UserIDList", string.Join(",", userIdList))
                        }
                    }.GetList<IssueData>(delegate (SqlDataReader reader) {
                        return CreateItem(reader);
                    });
                }

                return new List<IssueData>();
            }
        }
    }
}