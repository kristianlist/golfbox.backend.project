﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using GolfBox.Framework.Data;
using GolfBox.RM.Entities.User;

namespace GolfBox.RM.Data.User
{
    internal class UserDalc
    {
        /// <summary>
        /// This Method sets the corrosponding values from the database in the UserData object.
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static void SetValues(SqlDataReader reader, ref UserData data)
        {
            if (reader.HasRows)
            {
                data.ID = SqlReader.Read<int>(reader, "User_ID");
                data.Login = SqlReader.Read<string>(reader, "User_Login");
                data.Firstname = SqlReader.Read<string>(reader, "User_Firstname");
                data.Lastname = SqlReader.Read<string>(reader, "User_Lastname");
                data.Email = SqlReader.Read<string>(reader, "User_Email");                
            }
        }

        /// <summary>
        /// This Method constructs the UserData object.
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static UserData CreateItem(SqlDataReader reader)
        {
            UserData _outdata = new UserData();
            SetValues(reader, ref _outdata);
            return _outdata;
        }

        /// <summary>
        /// This Method inserts a UserData object and returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static UserData Insert(UserData indata)
        {
            return new Listor("[SSP.Redmine.Users.Insert]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@User_ID", indata.ID),
                    new Listor.Send<string>("@User_Login", indata.Login),
                    new Listor.Send<string>("@User_Firstname", indata.Firstname),
                    new Listor.Send<string>("@User_Lastname", indata.Lastname),
                    new Listor.Send<string>("@User_Email", indata.Email),
                }
            }.GetInstance<UserData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method updates a UserData object and returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static UserData Update(UserData indata)
        {
            return new Listor("[SSP.Redmine.Users.Update]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@User_ID", indata.ID),
                    new Listor.Send<string>("@User_Login", indata.Login),
                    new Listor.Send<string>("@User_Firstname", indata.Firstname),
                    new Listor.Send<string>("@User_Lastname", indata.Lastname),
                    new Listor.Send<string>("@User_Email", indata.Email),
                }
            }.GetInstance<UserData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method loads a UserData object and returns the loaded object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static UserData Load(int userID)
        {
            return new Listor("[SSP.Redmine.Users.Load]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@User_ID", userID)
                }
            }.GetInstance<UserData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        /// <summary>
        /// This Method deletes a UserData object and returns the deleted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        internal static UserData Delete(int userID)
        {
            return new Listor("[SSP.Redmine.Users.Delete]")
            {
                Params = new Listor.ISender[]{
                    new Listor.Send<int>("@User_ID", userID)
                }
            }.GetInstance<UserData>(delegate(SqlDataReader reader)
            {
                return CreateItem(reader);
            });
        }

        internal static class Lists
        {
            /// <summary>
            /// This Method gets a list of UserData
            /// </summary>
            /// <author>Sune Bruun Olsen</author>
            /// <created>18-12-2015</created>
            internal static List<UserData> All()
            {
                return new Listor("[SSP.Redmine.Users.Lists.All]").GetList<UserData>(delegate(SqlDataReader reader)
                {
                    return CreateItem(reader);
                });
            }
        }
    }
}