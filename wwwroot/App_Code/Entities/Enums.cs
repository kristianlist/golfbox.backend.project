﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GolfBox.RM.Entities.Enums
{
    public enum Version
    {
        Current = 69,
        Candidates = 70,
        Backlog = 82
    }

    public enum IssueError
    {
        AssigneeMustBeBlank = 1,
        AssigneeCannotBeBlank = 2,
        PctDoneMustBeZero = 5,
        TimeSpentMustBeZero = 6,
        EstimatedRiskTooHigh = 7,
        EstimatedHoursTooHigh = 8,
        EstimateMissing = 9,
        StatusMustBeNew = 10,
        StatusCannotBeNew = 11
    }

    public enum IssueStatus
    {
        New = 1,
        InProgress = 2,
        DoneNotCommitted = 9,
        CommittedToSVN = 16,
        UploadedTest = 7,
        UploadedProd = 13,
        Solved = 3,
        Duplicate = 14,
        Rejected = 6,
        Closed = 5
    }

    public enum IssueCondition
    {
        Green = 1,
        Yellow = 2,
        Red = 3
    }

}