﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GolfBox.RM.Entities.TimeEntry
{
    public class TimeEntryData
    {
        public int ID { get; set; }
        public decimal Hours { get; set; }
        public DateTime SpentOn { get; set; }
        public string Comment { get; set; }
        public int ProjectID { get; set; }
        public int IssueID { get; set; }
        public int UserID { get; set; }
        public int ActivityID { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public TimeEntryData()
        {

        }

    }
}




