﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GolfBox.RM.Entities.Project
{
    public class ProjectData
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Identifier { get; set; }
        public string Description { get; set; }
        public int? Parent_ID { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public ProjectData()
        {

        }

        public override string ToString() {
            return String.Format("#{0} {1}", this.ID, this.Name);
        }
    }
}
