﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GolfBox.RM.Entities.Issue {
    public class IssueData {
        public int ID { get; set; }
        public int? ParentID { get; set; }
        public string Subject { get; set; }
        ///
        public string Description { get; set; }
        public decimal? EstimatedHours { get; set; }
        public int? EstimatedRisk { get; set; }
        public int DoneRatio { get; set; }
        public int StatusID { get; set; }
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public int TrackerID { get; set; }
        public int PriorityID { get; set; }
        public DateTime? DueDate { get; set; }
        public DateTime? NextGroomingDate { get; set; }
        public int AuthorID { get; set; }
        public int? AssignedToID { get; set; }
        public string AssigneeName { get; set; }
        public string AssigneeInitals { get; set; }
        public int FixedVersionID { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        // Extended properties
        public List<IssueError> Errors { get; set; }
        public string FirstErrorText { get; set; }
        public decimal HoursSpent { get; set; }
        public decimal EstimatedRiskHours { get; set; } // Vi beregner hvor mange ekstra timer risiko faktor kan medføre 
        public decimal EstimatedLimitHours { get; set; } // Ud fra dette kan vi beregne vores max-grænse for antal estimerede timer
        public decimal LeftEstimatedHours { get; set; } // Vi beregner hvor mange af de normale estimerede timer, der er tilbage
        public decimal DoneEstimatedHours { get; set; } // Vi beregner hvor mange af de normale estimerede timer, der er færdige
        public decimal SavedEstimatedHours { get; set; } // Vi beregner hvor mange timer vi har sparet I forhold til estimatet
        public decimal DoneEstimatedLimitHours { get; set; } // Vi beregner hvor mange estimerede timer af vores max-grænse, der er færdige
        public decimal SavedEstimatedLimitHours { get; set; } // Vi beregner hvor mange timer vi har sparet I forhold til max-grænsen
        public decimal SavedEstimatedLimitRatio { get; set; } // Vi beregner hvor mange % vi har sparet I forhold til max-grænsen
        public GolfBox.RM.Entities.Enums.IssueCondition Condition { get; set; }

        public IssueData() {
            Errors = new List<IssueError>();
        }

        public string Link { get { return "<a href=\"http://redmine.golfbox.dk/issues/" + this.ID + "\" target=\"_blank\">" + this.ID + "</a>"; } }
        public string StatusName {
            get {
                switch (StatusID) {
                    case 1:
                        return "New";
                    case 2:
                        return "In progress";
                    case 9:
                        return "Done, Not committed";
                    case 16:
                        return "Committed to SVN";
                    case 7:
                        return "Uploaded TEST";
                    case 13:
                        return "Uploaded PROD";
                    case 11:
                        return "Test approved";
                    case 12:
                        return "Test not approved";
                    case 3:
                        return "Solved";
                    case 14:
                        return "Duplicate";
                    case 6:
                        return "Rejected";
                    case 5:
                        return "Closed";
                    default:
                        return "statusname";
                }
            }
        }

        public RulesControl ToRulesControl() {
            return new RulesControl {
                Link = this.Link,
                ProjectName = this.ProjectName,
                Subject = this.Subject,
                AssigneeInitals = this.AssigneeInitals,
                FirstErrorText = this.FirstErrorText
            };
        }
    }

    public class IssueError {
        public GolfBox.RM.Entities.Enums.IssueError Error { get; set; }
        public string Text { get; set; }

        public IssueError() {
        }
    }

    public class RulesControl {
        public string Link { get; set; }
        public string ProjectName { get; set; }
        public string Subject { get; set; }
        public string AssigneeInitals { get; set; }
        public string FirstErrorText { get; set; }
    }
}