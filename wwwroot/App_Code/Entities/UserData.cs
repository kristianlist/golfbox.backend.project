﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GolfBox.RM.Entities.User
{
    public class UserData
    {
        public int ID { get; set; }
        public string Login { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }

        // Extended properties
        public List<Issue.IssueData> AssignedIssues { get; set; }
        public decimal HoursSpentThisWeek { get; set; }
        public decimal HoursSpentLastWeek { get; set; }
        public decimal EstimatedHoursCurrent { get; set; }
        public decimal EstimatedHoursCandidates { get; set; }
        public int IssuesCurrent { get; set; }
        public int IssuesCandidates { get; set; }
        public string LatestSpentTime { get; set; }

        public UserData()
        {
            AssignedIssues = new List<Issue.IssueData>();
        }

        public string Fullname { get { return this.Firstname.ToString() + ((string.IsNullOrEmpty(this.Lastname) ? "" : " ")) + this.Lastname.ToString(); } }
        public string Initials { get { return this.Email.Substring(0, 2).ToUpper(); } }

        public override string ToString() {
            return String.Format("#{0} {1} {2}", this.ID, this.Firstname, this.Lastname);
        }
    }
}