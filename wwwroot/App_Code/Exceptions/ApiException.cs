﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GolfBox.RM.Exceptions
{
	public class ApiException : Exception
	{

		public ApiException()
		{

		}

		public ApiException(string message)
			: base(message)
		{
		}

		public ApiException(string message, Exception inner)
			: base(message, inner)
		{

		}

		public ApiException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
			: base(info, context)
		{

		}
	}
}
