﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GolfBox.RM.Exceptions
{


	[Serializable]
	public class NotifyException : Exception
	{

		public NotifyException()
		{

		}

		public NotifyException(string message)
			: base(message)
		{
		}

		public NotifyException(string message, Exception inner)
			: base(message, inner)
		{

		}

		public NotifyException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
			: base(info, context)
		{

		}
	}

}
