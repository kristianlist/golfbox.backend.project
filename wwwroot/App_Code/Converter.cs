using System;
using System.Globalization;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

public partial class Converter
{
    #region DateTime Methods

    public static DateTime TextToDate(string date)
    {
        return DateTime.ParseExact(date, "yyyyMMdd", CultureInfo.InvariantCulture);
    }

    public static string DateToText(DateTime date)
    {
        return date.ToString("yyyyMMdd", CultureInfo.InvariantCulture);
    }

    public static DateTime TextToDateTime(string dateTime)
    {
        return DateTime.ParseExact(dateTime, "yyyyMMdd\"T\"HHmmss", CultureInfo.InvariantCulture);
    }

    public static string DateTimeToText(DateTime dateTime)
    {
        return dateTime.ToString("yyyyMMdd\"T\"HHmmss", CultureInfo.InvariantCulture);
    }

    public static int DateTimeToWeekNumber(DateTime dateTime)
    {
        return CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(dateTime, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
    }

    #endregion

    #region Numeric Methods

    public static byte ToByte(string value)
    {
        byte result;
        if (byte.TryParse(value, out result))
            return result;
        return byte.MinValue;
    }

    public static short ToInt16(string value)
    {
        short result;
        if (short.TryParse(value, out result))
            return result;
        return 0;
    }

    public static int ToInt(string value)
    {
        int result;
        if (int.TryParse(value, out result))
            return result;
        return 0;
    }

    public static int? ToNullableInt(string value)
    {
        int result;
        if (int.TryParse(value, out result))
            return result;
        return new int?();
    }

    public static long ToLong(string value)
    {
        long result;
        if (long.TryParse(value, out result))
            return result;
        return 0L;
    }

    public static float ToFloat(string value)
    {
        float result;
        if (float.TryParse(value, out result))
            return result;
        return 0f;
    }

    public static double ToDouble(string value)
    {
        double result;
        if (double.TryParse(value.Replace(",", "."), NumberStyles.Number, new System.Globalization.CultureInfo("en-GB"), out result))
            return result;
        return 0.0;
    }

    public static decimal ToDecimal(string value)
    {
        decimal result;
        if (decimal.TryParse(value.Replace(",", "."), NumberStyles.Number, new System.Globalization.CultureInfo("en-GB"), out result))
            return result;
        return decimal.Zero;
    }

    #endregion

    #region Hcp Methods

    public static decimal TextToHcp(string hcp)
    {
        return TextToHcp(hcp, CultureInfo.CurrentCulture);
    }

    public static decimal TextToHcp(string hcp, CultureInfo culture)
    {
        decimal result;
        if (decimal.TryParse((hcp ?? "").Replace("+", "-"), NumberStyles.AllowLeadingSign | NumberStyles.AllowTrailingSign | NumberStyles.AllowDecimalPoint, culture, out result))
            return result;
        return decimal.MinValue;
    }

    public static string HcpToText(string hcp)
    {
        return HcpToText(hcp, CultureInfo.CurrentCulture);
    }

    public static string HcpToText(decimal hcp)
    {
        return HcpToText(hcp, CultureInfo.CurrentCulture);
    }

    public static string HcpToText(decimal hcp, CultureInfo culture)
    {
        decimal tHcp = hcp;
        string format = (tHcp >= 37m) ? "F0" : "F1";

        if (tHcp < 0m)
            return "+" + Math.Abs(tHcp).ToString(format, culture);

        if (tHcp > 36m && Regex.IsMatch(tHcp.ToString("F2", culture), "(,|\\.)[1-9]"))
            format = "F1";

        return tHcp.ToString(format, culture);
    }

    public static string HcpToText(string hcp, CultureInfo culture)
    {
        decimal result = TextToHcp(hcp, culture);
        if (result != decimal.MinValue)
            return HcpToText(result, culture);
        return string.Empty;
    }

    #endregion

    #region Misc. Methods

    public static string ToXml(object obj)
    {
        StringBuilder sb = new StringBuilder();
        using (StringWriter sw = new StringWriter(sb))
        {
            XmlSerializer xs = new XmlSerializer(obj.GetType());
            xs.Serialize(sw, obj);
        }
        return sb.ToString();
    }

    public static bool ToBool(object obj)
    {
        if (!IsEmpty(obj))
        {
            if (obj is string)
            {
                string value = (obj as string).Trim().ToLower();

                if (value == "on" ||
                    value == "yes" ||
                    value == "true" ||
                    ToDouble(value) > 0.0)
                    return true;

                return false;
            }

            Type t = obj.GetType();

            if (t.IsValueType)
                return !obj.Equals(Activator.CreateInstance(t));
        }

        return false;
    }

    public static string ToJson(object obj)
    {
        return ToJson(obj, false);
    }

    public static string ToJson(object obj, bool indented)
    {
        Newtonsoft.Json.JsonSerializerSettings setting = new Newtonsoft.Json.JsonSerializerSettings();
        Newtonsoft.Json.Converters.IsoDateTimeConverter isoDate = new Newtonsoft.Json.Converters.IsoDateTimeConverter()
        {
            DateTimeFormat = "yyyyMMdd\"T\"HHmmsszzz"
        };
        setting.Converters.Add(isoDate);
        return Newtonsoft.Json.JsonConvert.SerializeObject(obj, indented ? Newtonsoft.Json.Formatting.Indented : Newtonsoft.Json.Formatting.None, setting);
    }

    public static T FromJson<T>(string json)
    {
        Newtonsoft.Json.JsonSerializerSettings setting = new Newtonsoft.Json.JsonSerializerSettings();
        Newtonsoft.Json.Converters.IsoDateTimeConverter isoDate = new Newtonsoft.Json.Converters.IsoDateTimeConverter()
        {
            DateTimeFormat = "yyyyMMdd\"T\"HHmmsszzz",
            DateTimeStyles = DateTimeStyles.AssumeUniversal
        };
        setting.Converters.Add(isoDate);
        return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json, setting);
    }

    #endregion

    #region Utilities

    public static bool IsEmpty(object obj)
    {
        if (obj == null || obj == DBNull.Value)
            return true;

        if (obj is string)
            return string.IsNullOrWhiteSpace(obj as string);

        var t = obj.GetType();

        if (t.IsGenericType)
            return (int)t.GetProperty("Count").GetValue(obj, null) == 0;

        if (t.IsArray)
            return (int)t.GetProperty("Length").GetValue(obj, null) == 0;

        return t.IsValueType && obj.Equals(Activator.CreateInstance(t));
    }

    public static bool IsInteger(ValueType value)
    {
        return (value is sbyte ||
                value is byte ||
                value is short ||
                value is int ||
                value is long ||
                value is ushort ||
                value is uint ||
                value is ulong);
    }

    public static bool IsFloat(ValueType value)
    {
        return (value is float ||
                value is double ||
                value is decimal);
    }

    public static bool IsNumeric(ValueType value)
    {
        return IsInteger(value) || IsFloat(value);
    }

    #endregion
}
