using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using System.Net;

public class External
{

    private static object ProcessRedmineApiRequest(string uri, string method, string format)
    {
        string server = GolfBox.Framework.ConfigReader.ReadConfig("Redmine.API.ServerPath");
        string key = System.Configuration.ConfigurationManager.AppSettings["Redmine.API.Key"];

        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(server + '/' + uri);
        HttpWebRequest.DefaultWebProxy = null;

        req.Headers.Add("X-Redmine-API-Key", key);
        req.ContentType = "application/x-www-form-urlencoded";
        req.Method = method;
        req.Accept = format;

        WebResponse response;
        try
        {
            response = req.GetResponse();
            object obj = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();

            response.Close();

            return obj;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    #region Methods

    public static ProjectsJSON GetProjects()
    {
        string requestUri = "projects.json?limit=100&sort=updated_on:desc";
        object obj = ProcessRedmineApiRequest(requestUri, "GET", "application/json");

        if (obj != null)
        {
            ProjectsJSON json = GolfBox.Framework.Core.Json.JsonHandlers.DeserializeJson<ProjectsJSON>(obj.ToString());
            return json;
        }
        else
        {
            return null;
        }
    }

    public static IssuesJSON GetIssues(int offset = 0)
    {
        string requestUri = string.Format("issues.json?status_id=*&sort=updated_on:desc&limit=100&offset={0}", offset.ToString());
        object obj = ProcessRedmineApiRequest(requestUri, "GET", "application/json");

        if (obj != null)
        {
            IssuesJSON json = GolfBox.Framework.Core.Json.JsonHandlers.DeserializeJson<IssuesJSON>(obj.ToString());
            return json;
        }
        else
        {
            return null;
        }
    }

    public static TimeEntriesJSON GetTimeEntries(int offset = 0)
    {
        string requestUri = string.Format("time_entries.json?sort=spent_on:desc&limit=100&offset={0}", offset.ToString());
        object obj = ProcessRedmineApiRequest(requestUri, "GET", "application/json");

        if (obj != null)
        {
            TimeEntriesJSON json = GolfBox.Framework.Core.Json.JsonHandlers.DeserializeJson<TimeEntriesJSON>(obj.ToString());
            return json;
        }
        else
        {
            return null;
        }
    }

    public static UsersJSON GetUsers()
    {
        string requestUri = "users.json?limit=100";
        object obj = ProcessRedmineApiRequest(requestUri, "GET", "application/json");

        if (obj != null)
        {
            UsersJSON json = GolfBox.Framework.Core.Json.JsonHandlers.DeserializeJson<UsersJSON>(obj.ToString());
            return json;
        }
        else
        {
            return null;
        }
    }

    public static IssuesJSON GetQuery(int queryID)
    {
        string requestUri = string.Format("issues.json?query_id={0}&limit=1", queryID);
        object obj = ProcessRedmineApiRequest(requestUri, "GET", "application/json");

        if (obj != null)
        {
            IssuesJSON json = GolfBox.Framework.Core.Json.JsonHandlers.DeserializeJson<IssuesJSON>(obj.ToString());
            return json;
        }
        else
        {
            return null;
        }
    }
    #endregion

    #region Classes

    public class ProjectsJSON
    {
        public List<ExtendedProject> projects { get; set; }
        public int total_count { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
    }

    public class Project
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class ExtendedProject : Project
    {
        public string identifier { get; set; }
        public string description { get; set; }
        public Project parent { get; set; }
        public DateTimeOffset created_on { get; set; }
        public DateTimeOffset updated_on { get; set; }
    }

    public class IssuesJSON
    {
        public List<ExtendedIssue> issues { get; set; }
        public int total_count { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
    }

    public class Issue
    {
        public int id { get; set; }
    }

    public class ExtendedIssue : Issue
    {
        public Project project { get; set; }
        public Tracker tracker { get; set; }
        public Status status { get; set; }
        public Priority priority { get; set; }
        public User author { get; set; }
        public User assigned_to { get; set; }
        public Category category { get; set; }
        public FixedVersion fixed_version { get; set; }
        public Issue parent { get; set; }
        public string subject { get; set; }
        public string description { get; set; }
        public int done_ratio { get; set; }
        public decimal estimated_hours { get; set; }
        public List<CustomField> custom_fields { get; set; }
        public string start_date { get; set; }
        public string due_date { get; set; }
        public DateTimeOffset created_on { get; set; }
        public DateTimeOffset updated_on { get; set; }
    }

    public class TimeEntriesJSON
    {
        public List<TimeEntry> time_entries { get; set; }
        public int total_count { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
    }

    public class TimeEntry
    {
        public int id { get; set; }
        public Project project { get; set; }
        public Issue issue { get; set; }
        public User user { get; set; }
        public Activity activity { get; set; }
        public decimal hours { get; set; }
        public string spent_on { get; set; }
        public string comments { get; set; }
        public DateTimeOffset created_on { get; set; }
        public DateTimeOffset updated_on { get; set; }
    }

    public class UsersJSON
    {
        public List<ExtendedUser> users { get; set; }
        public int total_count { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
    }

    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class ExtendedUser : User
    {
        public string login { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string mail { get; set; }
        public DateTimeOffset created_on { get; set; }
        //public DateTimeOffset last_login_on { get; set; }
    }

    public class Activity
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Tracker
    {
        public int  id { get; set; }
        public string name { get; set; }
    }

    public class Status
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Priority
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class FixedVersion
    {
        public int id { get; set; }
        public string name { get; set; }
    }

    public class CustomField
    {
        public int id { get; set; }
        public string name { get; set; }
        public string value { get; set; }
    }

    #endregion
}
