namespace WebSite
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Net;
    using System.Configuration;
    using System.Web.SessionState;
    using System.Collections.Specialized;
    using System.Text;
    using System.Globalization;
    using ent = GolfBox.RM.Entities;
    using GolfBox.RM.Entities.Issue;
    using GolfBox.RM.Entities.Project;
    using GolfBox.RM.Entities.User;
    using GolfBox.RM.Entities.TimeEntry;
    using GolfBox.Framework.Core;
    using System.Threading.Tasks;

    public class Handler : IHttpHandler, IReadOnlySessionState
    {
        public bool IsReusable
        {
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            switch (context.Request.PathInfo.Replace("/", "").ToLower())
            {
                case "updatefromredmine":
                    Method_UpdateFromRedmine(context);
                    return;
                case "getrulescontrol":
                    Method_GetRulesControl(context);
                    return;
                case "getusers":
                    Method_GetUsers(context);
                    return;
                case "getcurrentissues":
                    Method_GetCurrentIssues(context);
                    return;
            }
            WebSite.Helper.WriteJsonToClient(context, "");
        }

        private void Method_UpdateFromRedmine(HttpContext context)
        {
            int projectsUpdated;
            GolfBox.RM.Logic.Project.ProjectLogic.UpdateFromExternal(out projectsUpdated);
            int usersUpdated;
            GolfBox.RM.Logic.User.UserLogic.UpdateFromExternal(out usersUpdated);
            int issuesUpdated;
            GolfBox.RM.Logic.Issue.IssueLogic.UpdateFromExternal(out issuesUpdated);
            int timeEntriesUpdated;
            GolfBox.RM.Logic.TimeEntry.TimeEntryLogic.UpdateFromExternal(out timeEntriesUpdated);

            object[] placeholder = new object[] { "Issues Updated:" + issuesUpdated.ToString(), "\nTime Entries Updated:" + timeEntriesUpdated.ToString() };

            WebSite.Helper.WriteJsonToClient(context, placeholder);

        }
 
        private void Method_GetRulesControl(HttpContext context)
        {
            //List<ent.Enums.IssueStatus> closedStatuses = new List<ent.Enums.IssueStatus>(4) { ent.Enums.IssueStatus.Closed, ent.Enums.IssueStatus.Rejected, ent.Enums.IssueStatus.Duplicate, ent.Enums.IssueStatus.UploadedProd };
            List<ProjectData> projects = GolfBox.RM.Logic.Project.ProjectLogic.Lists.All();
            List<UserData> users = GolfBox.RM.Logic.User.UserLogic.Lists.All();

            #region Current Issues
            List<IssueData> currentIssues = GolfBox.RM.Logic.Issue.IssueLogic.Lists.ByVersionAndOpen(ent.Enums.Version.Current, true)
                .Where(e => e.DoneRatio < 100)
                .ToListOrEmpty();

            // Load TimeEntries to find out how many hours have been spent on each issue
            ILookup<int, TimeEntryData> CurrentTimeEntries = GolfBox.RM.Logic.TimeEntry.TimeEntryLogic.Lists.ByVersionAndOpen(ent.Enums.Version.Current, true)
                .ToLookup(e => e.IssueID);

            currentIssues.ForEach(x => {
                // Current issues must have an assignee
                if (!x.AssignedToID.HasValue || x.AssignedToID == 0) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.AssigneeCannotBeBlank, Text = "No Assignee" });
                }

                // Estimated Risk must be below 30%
                if (!x.EstimatedRisk.HasValue || x.EstimatedRisk.Value >= 30) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.EstimatedRiskTooHigh, Text = String.Format("Risk of {0} is too high", x.EstimatedRisk.ToString()) });
                }

                // Estimated Hours must be maximum 20 hours
                if (x.EstimatedHours.HasValue && x.EstimatedHours.Value > 20) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.EstimatedHoursTooHigh, Text = String.Format("Estimate of {0} hours is too high", x.EstimatedHours.ToString()) });
                }

                // Estimated Hours missing
                if (!x.EstimatedHours.HasValue || x.EstimatedHours.Value == 0) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.EstimateMissing, Text = "Estimate missing" });
                }

                // Status cannot be NEW
                x.HoursSpent = CurrentTimeEntries[x.ID].Sum(z => z.Hours);
                if (x.StatusID == 1 && (x.HoursSpent > 0 || x.DoneRatio > 0)) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.StatusCannotBeNew, Text = "Status cannot be NEW" });
                }

                // Only do settes for keepers
                if (x.Errors.Count > 0) {
                    x.ProjectName = projects.Find(y => y.ID == x.ProjectID).Name;
                    x.FirstErrorText = x.Errors.First().Text;

                    if (x.AssignedToID == null || x.AssignedToID == 0) {
                        x.AssigneeName = x.AssigneeInitals = "";
                    } else {
                        UserData user = users.Find(y => y.ID == x.AssignedToID);
                        x.AssigneeName = user.Fullname;
                        x.AssigneeInitals = user.Initials;
                    }
                    x.AssigneeName = (x.AssignedToID == null || x.AssignedToID == 0 ? "" : users.Find(y => y.ID == x.AssignedToID).Fullname);
                    x.AssigneeInitals = (x.AssignedToID == null || x.AssignedToID == 0 ? "" : users.Find(y => y.ID == x.AssignedToID).Initials);
                }
            });

            currentIssues.RemoveAll(x => x.Errors.Count == 0);

            currentIssues = currentIssues.OrderBy(x => x.AssigneeInitals).ThenBy(y => y.ID).ToList();
            #endregion

            #region Candidate Issues
            List<IssueData> candidateIssues = GolfBox.RM.Logic.Issue.IssueLogic.Lists.ByVersionAndOpen(ent.Enums.Version.Candidates, true);

            // Load TimeEntries to find out how many hours have been spent on each issue
            ILookup<int, TimeEntryData> CandidateTimeEntries = GolfBox.RM.Logic.TimeEntry.TimeEntryLogic.Lists.ByVersionAndOpen(ent.Enums.Version.Candidates, true)
                .ToLookup(e => e.IssueID);

            candidateIssues.ForEach(x => {
                // Candidate issues must have an assignee
                if (x.AssignedToID == null || x.AssignedToID == 0) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.AssigneeCannotBeBlank, Text = "No Assignee" });
                }

                // Pct Done must be zero
                if (x.DoneRatio > 0) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.PctDoneMustBeZero, Text = String.Format("Pct Done of {0}% must be zero", x.DoneRatio.ToString()) });
                }

                // Status must be NEW
                if (x.StatusID != 1) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.StatusMustBeNew, Text = "Status must be NEW" });
                }

                // Cant have hours spent
                x.HoursSpent = CandidateTimeEntries[x.ID].Sum(z => z.Hours);
                if (x.HoursSpent > 0) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.TimeSpentMustBeZero, Text = String.Format("Time spent of {0} hours must be zero", x.HoursSpent.ToString()) });
                }

                // Only do settes for keepers
                if (x.Errors.Count > 0) {
                    x.ProjectName = projects.Find(y => y.ID == x.ProjectID).Name;
                    x.FirstErrorText = x.Errors.First().Text;

                    if (x.AssignedToID == null || x.AssignedToID == 0) {
                        x.AssigneeName = x.AssigneeInitals = "";
                    } else {
                        UserData user = users.Find(y => y.ID == x.AssignedToID);
                        x.AssigneeName = user.Fullname;
                        x.AssigneeInitals = user.Initials;
                    }
                    x.AssigneeName = (x.AssignedToID == null || x.AssignedToID == 0 ? "" : users.Find(y => y.ID == x.AssignedToID).Fullname);
                    x.AssigneeInitals = (x.AssignedToID == null || x.AssignedToID == 0 ? "" : users.Find(y => y.ID == x.AssignedToID).Initials);
                }
            });

            candidateIssues.RemoveAll(x => x.Errors.Count == 0);

            candidateIssues = candidateIssues.OrderBy(x => x.AssigneeInitals).ThenBy(y => y.ID).ToList();
            #endregion

            #region Backlog Issues
            List<IssueData> backlogIssues = GolfBox.RM.Logic.Issue.IssueLogic.Lists.ByVersionAndOpen(ent.Enums.Version.Backlog, true);

            // Load TimeEntries to find out how many hours have been spent on each issue
            ILookup<int, TimeEntryData> BacklogTimeEntries = GolfBox.RM.Logic.TimeEntry.TimeEntryLogic.Lists.ByVersionAndOpen(ent.Enums.Version.Backlog, true)
                .ToLookup(e => e.IssueID);

            backlogIssues.ForEach(x => {
                // Backlog issues cannot have have an assignee
                if (x.AssignedToID.HasValue && x.AssignedToID > 0) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.AssigneeMustBeBlank, Text = "Assignee must be blank" });
                }

                // Pct Done must be zero
                if (x.DoneRatio > 0) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.PctDoneMustBeZero, Text = String.Format("Pct Done of {0}% must be zero", x.DoneRatio.ToString()) });
                }

                // Status must be NEW
                if (x.StatusID != 1) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.StatusMustBeNew, Text = "Status must be NEW" });
                }

                // Can't have hours spent
                x.HoursSpent = BacklogTimeEntries[x.ID].Sum(z => z.Hours);
                if (x.HoursSpent > 0) {
                    x.Errors.Add(new IssueError() { Error = ent.Enums.IssueError.TimeSpentMustBeZero, Text = String.Format("Time spent of {0} hours must be zero", x.HoursSpent.ToString()) });
                }

                // Only do settes for keepers
                if (x.Errors.Count > 0) {
                    x.ProjectName = projects.Find(y => y.ID == x.ProjectID).Name;
                    x.FirstErrorText = x.Errors.First().Text;

                    if (x.AssignedToID == null || x.AssignedToID == 0) {
                        x.AssigneeName = x.AssigneeInitals = "";
                    } else {
                        UserData user = users.Find(y => y.ID == x.AssignedToID);
                        x.AssigneeName = user.Fullname;
                        x.AssigneeInitals = user.Initials;
                    }
                    x.AssigneeName = (x.AssignedToID == null || x.AssignedToID == 0 ? "" : users.Find(y => y.ID == x.AssignedToID).Fullname);
                    x.AssigneeInitals = (x.AssignedToID == null || x.AssignedToID == 0 ? "" : users.Find(y => y.ID == x.AssignedToID).Initials);
                }
            });

            backlogIssues.RemoveAll(x => x.Errors.Count == 0);

            backlogIssues = backlogIssues.OrderBy(x => x.ProjectName).ThenBy(y => y.ID).ToList();
            #endregion

            WebSite.Helper.WriteJsonToClient(context, new object[3] {
                currentIssues.Select(e => e.ToRulesControl()),
                candidateIssues.Select(e => e.ToRulesControl()),
                backlogIssues.Select(e => e.ToRulesControl())
            });
        }

        private void Method_GetUsers(HttpContext context)
        {
            DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
            Calendar cal = dfi.Calendar;
            int thisWeekNumber = cal.GetWeekOfYear(DateTime.Now, dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
            int thisWeekYear = DateTime.Now.Year;
            int lastWeekNumber = cal.GetWeekOfYear(DateTime.Now.AddDays(-7), dfi.CalendarWeekRule, dfi.FirstDayOfWeek);
            int lastWeekYear = DateTime.Now.AddDays(-7).Year;

            List<ProjectData> projects = GolfBox.RM.Logic.Project.ProjectLogic.Lists.All();
            List<UserData> users = GolfBox.RM.Logic.User.UserLogic.Lists.All();
            List<int> users_to_remove = new List<int>() {
                04, //MG
                06, //MH
                14, //PH
                21, //PF
                22, //KB
                24, //CO
                25, //RS
                36, //MB
                37, //AL
                40, //Mail
                55, //CL
                61, //ME
                65, //Adrian
                68, //EO
                71, //Nick H�ttel
                74, //Jason Hold
                75, //Kristin Kl�bo
                77, //Jonas Kjeldgaard
                78, //GolfBox.Project.Backend
            };

            users.RemoveAll(x => {
                bool remove = false;
                if (users_to_remove.Any(e => x.ID == e)) {
                    remove = true;
                    users_to_remove.Remove(x.ID); // So not to check again
                }
                return remove;
            });

            List<TimeEntryData> timeEntries = GolfBox.RM.Logic.TimeEntry.TimeEntryLogic.Lists.ByUserList(users.Select(x => x.ID).ToList());
            List<IssueData> assignedIssues = GolfBox.RM.Logic.Issue.IssueLogic.Lists.ByAssigneeList(users.Select(x => x.ID).ToList());

            // Remove closed issues
            assignedIssues.RemoveAll(x => (ent.Enums.IssueStatus)x.StatusID == ent.Enums.IssueStatus.Closed);
            assignedIssues.RemoveAll(x => (ent.Enums.IssueStatus)x.StatusID == ent.Enums.IssueStatus.Rejected);
            assignedIssues.RemoveAll(x => (ent.Enums.IssueStatus)x.StatusID == ent.Enums.IssueStatus.Duplicate);
            assignedIssues.RemoveAll(x => (ent.Enums.IssueStatus)x.StatusID == ent.Enums.IssueStatus.UploadedProd);
            assignedIssues.ForEach(x => x.ProjectName = projects.Find(y => y.ID == x.ProjectID).Name);

            // Load TimeEntries to find out how many hours have been spent on each issue
            List<TimeEntryData> CurrentTimeEntries = GolfBox.RM.Logic.TimeEntry.TimeEntryLogic.Lists.ByIssueList(assignedIssues.Select(x => x.ID).Distinct().ToList(), true);
            assignedIssues.ForEach(x => x.HoursSpent = CurrentTimeEntries.Where(y => y.IssueID == x.ID).Sum(z => z.Hours));
            assignedIssues.ForEach(x => x.DoneEstimatedHours = (x.EstimatedHours.Value * (decimal)x.DoneRatio / 100));
            assignedIssues.ForEach(x => x.LeftEstimatedHours = (x.EstimatedHours.Value - x.DoneEstimatedHours));

            users.ForEach(x => x.AssignedIssues.AddRange(assignedIssues.Where(y => y.AssignedToID == x.ID)));
            users.ForEach(x => x.IssuesCurrent = x.AssignedIssues.Where(y => y.FixedVersionID == (int)ent.Enums.Version.Current && y.DoneRatio < 100).Count());
            users.ForEach(x => x.IssuesCandidates = x.AssignedIssues.Where(y => y.FixedVersionID == (int)ent.Enums.Version.Candidates && y.DoneRatio < 100).Count());
            users.ForEach(x => x.EstimatedHoursCurrent = x.AssignedIssues.Where(y => y.FixedVersionID == (int)ent.Enums.Version.Current).Sum(z => z.LeftEstimatedHours));
            users.ForEach(x => x.EstimatedHoursCandidates = x.AssignedIssues.Where(y => y.FixedVersionID == (int)ent.Enums.Version.Candidates).Sum(z => z.LeftEstimatedHours));
            users.ForEach(x => x.HoursSpentThisWeek = timeEntries.Where(y => y.UserID == x.ID && y.SpentOn.Year == thisWeekYear && cal.GetWeekOfYear(y.SpentOn, dfi.CalendarWeekRule, dfi.FirstDayOfWeek) == thisWeekNumber).Sum(z => z.Hours));
            users.ForEach(x => x.HoursSpentLastWeek = timeEntries.Where(y => y.UserID == x.ID && y.SpentOn.Year == lastWeekYear && cal.GetWeekOfYear(y.SpentOn, dfi.CalendarWeekRule, dfi.FirstDayOfWeek) == lastWeekNumber).Sum(z => z.Hours));
            users.ForEach(x => x.LatestSpentTime = DateTime.UtcNow.ToUniversalTime().Subtract((timeEntries.Where(y => y.UserID == x.ID).Max(z => z.UpdatedOn)).GetValueOrDefault(DateTime.UtcNow.ToUniversalTime())).ToString(@"dd' days 'hh' hours'"));
            users = users.OrderByDescending(x => DateTime.UtcNow.ToUniversalTime().Subtract((timeEntries.Where(y => y.UserID == x.ID).Max(z => z.UpdatedOn)).GetValueOrDefault(DateTime.UtcNow.ToUniversalTime()))).ToList();

            WebSite.Helper.WriteJsonToClient(context, users);
        }

        private void Method_GetCurrentIssues(HttpContext context)
        {
            List<ProjectData> projects = GolfBox.RM.Logic.Project.ProjectLogic.Lists.All();
            List<UserData> users = GolfBox.RM.Logic.User.UserLogic.Lists.All();
            List<IssueData> currentIssues = GolfBox.RM.Logic.Issue.IssueLogic.Lists.ByVersion(ent.Enums.Version.Current, true);

            // Remove closed issues
            currentIssues.RemoveAll(x => (ent.Enums.IssueStatus)x.StatusID == ent.Enums.IssueStatus.Closed);
            currentIssues.RemoveAll(x => (ent.Enums.IssueStatus)x.StatusID == ent.Enums.IssueStatus.Rejected);
            currentIssues.RemoveAll(x => (ent.Enums.IssueStatus)x.StatusID == ent.Enums.IssueStatus.Duplicate);
            currentIssues.RemoveAll(x => (ent.Enums.IssueStatus)x.StatusID == ent.Enums.IssueStatus.UploadedProd);
            currentIssues.RemoveAll(x => x.DoneRatio > 75);

            currentIssues.ForEach(x => x.ProjectName = projects.Find(y => y.ID == x.ProjectID).Name);
            currentIssues.ForEach(x => x.AssigneeName = (x.AssignedToID == null || x.AssignedToID == 0 ? "" : users.Find(y => y.ID == x.AssignedToID).Fullname));
            currentIssues.ForEach(x => x.AssigneeInitals = (x.AssignedToID == null || x.AssignedToID == 0 ? "" : users.Find(y => y.ID == x.AssignedToID).Initials));

            // Load TimeEntries to find out how many hours have been spent on each issue
            List<TimeEntryData> CurrentTimeEntries = GolfBox.RM.Logic.TimeEntry.TimeEntryLogic.Lists.ByIssueList(currentIssues.Select(x => x.ID).Distinct().ToList(), true);
            currentIssues.ForEach(x => x.HoursSpent = CurrentTimeEntries.Where(y => y.IssueID == x.ID).Sum(z => z.Hours));

            currentIssues.ForEach(x => x.EstimatedRiskHours = (x.EstimatedHours.Value * (decimal)x.EstimatedRisk.Value / 100));
            currentIssues.ForEach(x => x.EstimatedLimitHours = (x.EstimatedHours.Value + x.EstimatedRiskHours));
            currentIssues.ForEach(x => x.DoneEstimatedHours = (x.EstimatedHours.Value * (decimal)x.DoneRatio / 100));
            currentIssues.ForEach(x => x.LeftEstimatedHours = (x.EstimatedHours.Value - x.DoneEstimatedHours));
            currentIssues.ForEach(x => x.SavedEstimatedHours = (x.DoneEstimatedHours - x.HoursSpent));
            currentIssues.ForEach(x => x.DoneEstimatedLimitHours = (x.EstimatedLimitHours * (decimal)x.DoneRatio / 100));
            currentIssues.ForEach(x => x.SavedEstimatedLimitHours = (x.DoneEstimatedLimitHours - x.HoursSpent));
            currentIssues.ForEach(x => x.SavedEstimatedLimitRatio = (100/(x.EstimatedLimitHours > 0 ? x.EstimatedLimitHours : 1) * x.SavedEstimatedLimitHours));
            currentIssues.ForEach(x => x.Condition = (x.SavedEstimatedHours >= 0 ? ent.Enums.IssueCondition.Green : (x.SavedEstimatedLimitHours < 0 ? ent.Enums.IssueCondition.Red : ent.Enums.IssueCondition.Yellow)));
            currentIssues = currentIssues.OrderBy(x => x.SavedEstimatedLimitRatio).ThenBy(y => y.ID).ToList();

            currentIssues.RemoveAll(x => x.LeftEstimatedHours < 2);

            object[] placeholder = new object[] { currentIssues.Where(x => x.Condition == ent.Enums.IssueCondition.Red).ToList(), currentIssues.Where(x => x.Condition == ent.Enums.IssueCondition.Yellow).ToList() };

            WebSite.Helper.WriteJsonToClient(context, placeholder);
        }
    }
}