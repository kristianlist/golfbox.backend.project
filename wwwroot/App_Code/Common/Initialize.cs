namespace WebSite.Common
{
	using System;
	using System.Web;

	public class Initialize
	{
		#region Fields
		private static readonly object _InitializeLock = new object();
		private static bool _InitializedAlready;
		#endregion

		#region Properties

		public static DateTime InitializedDateTime { get; set; }
		public static string IISAppName { get; set; }
		public static string ServerName { get; set; }
		public static Version OperatingSystemVersion { get; set; }
		public static Version NETFrameworkVersion { get; set; }

		#endregion

		#region Methods

		public static void Init(HttpApplication app)
		{
			if (_InitializedAlready)
				return;

			lock (_InitializeLock)
			{
				if (_InitializedAlready)
					return;

				InitializeApp(app);
				_InitializedAlready = true;
			}
		}

		#endregion

		#region Utilities

		private static void InitializeApp(HttpApplication app)
		{
			InitializedDateTime    = DateTime.Now;
			IISAppName             = app.Request.ServerVariables["APPL_MD_PATH"];
			OperatingSystemVersion = Environment.OSVersion.Version;
			NETFrameworkVersion    = GetNETFrameworkVersion();

			app.Context.Items.Add("FirstRequest", true);
		}

		private static Version GetNETFrameworkVersion()
		{
			string version = Environment.Version.ToString(2);

			if (version == "2.0")
			{
				//Try and load a 3.0 Assembly
				try
				{
					AppDomain.CurrentDomain.Load("System.Runtime.Serialization, Version=3.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089");
					version = "3.0";
				}
				catch { }

				//Try and load a 3.5 Assembly
				try
				{
					AppDomain.CurrentDomain.Load("System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089");
					version = "3.5";
				}
				catch { }
			}

			return new Version(version);
		}

		#endregion
	}
}
