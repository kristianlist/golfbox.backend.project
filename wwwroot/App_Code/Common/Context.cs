namespace WebSite.Common
{
    using System.Linq;
	using System.Collections.Generic;
	using System.Globalization;
	using System.Threading;
	using System.Web;
    using System;

	public partial class Context
	{
		#region Fields
		private readonly HttpContext _Context = HttpContext.Current;
		private readonly Dictionary<string, object> _Dict = new Dictionary<string, object>();
		private WebSite.Common.Locale.ClientCulture _Culture;
		#endregion

		#region Properties
		public Locale.ClientCulture Culture
		{
			get
			{
				if (_Culture == null)
				{
					_Culture = new Locale.ClientCulture();
					_Culture.Load();
				}

				return _Culture;
			}
		}

		public string UserHostAddress
		{
			get
			{
				if (_Context != null && _Context.Request.UserHostAddress != null)
					return _Context.Request.UserHostAddress;
				return string.Empty;
			}
		}

		public static Context Current
		{
			get
			{
				if (HttpContext.Current.Items["WebSite.Common.Context"] == null)
				{
					Context ctx = new Context();
					HttpContext.Current.Items.Add("WebSite.Common.Context", ctx);
					return ctx;
				}

				return HttpContext.Current.Items["WebSite.Common.Context"] as Context;
			}
		}

		public object this[string key]
		{
			get
			{
				object obj;
				if (_Dict.TryGetValue(key, out obj))
					return obj;
				return null;
			}
			set
			{
				if (value == null)
					_Dict.Remove(key);
				else
					_Dict[key] = value;
			}
		}

		#endregion

		#region Ctor

		private Context()
		{
		}

		#endregion

		#region Methods

		#endregion
	}
}