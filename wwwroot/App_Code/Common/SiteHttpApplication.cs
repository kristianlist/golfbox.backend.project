namespace WebSite.Common
{
	using System;
	using System.Net;
	using System.Web;

	public class SiteHttpApplication : HttpApplication
	{
		void Application_Start(object sender, EventArgs e)
		{
			Initialize.ServerName = Dns.GetHostName();
		}

		void Application_End(object sender, EventArgs e)
		{
		}

		void Session_Start(object sender, EventArgs e)
		{
			//Store a dummy value. This causes the Session IDs to remain static, otherwise they keep changing
			if (Session != null)
				Session["dummy"] = 1;
		}

		void Session_End(object sender, EventArgs e)
		{
		}
	}
}