namespace WebSite.Common
{
	using System.Data;
	using System.Data.SqlClient;
	using System.Data.SqlTypes;
	using System.IO;
	using System.Text;
	using System.Web;
	using System.Xml;
    using System;

	public sealed class Utility
	{
		#region Methods

		public static string QueryString(string name)
		{
			if (HttpContext.Current != null && HttpContext.Current.Request.QueryString[name] != null)
				return HttpContext.Current.Request.QueryString[name];
			return string.Empty;
		}

		public static string FormString(string name)
		{
			if (HttpContext.Current != null && HttpContext.Current.Request.Form[name] != null)
				return HttpContext.Current.Request.Form[name];
			return string.Empty;
		}

		public static string RequestString(string name)
		{
			if (HttpContext.Current != null && HttpContext.Current.Request[name] != null)
				return HttpContext.Current.Request[name];
			return string.Empty;
		}

		public static string CookieString(string name)
		{
			if (HttpContext.Current != null && HttpContext.Current.Request.Cookies[name] != null)
				return HttpContext.Current.Request.Cookies[name].Value;
			return string.Empty;
		}

		public static string ServerVariables(string name)
		{
			if (HttpContext.Current != null && HttpContext.Current.Request.ServerVariables[name] != null)
				return HttpContext.Current.Request.ServerVariables[name];
			return string.Empty;
		}

        public static bool isAjaxRequest()
        {
            var request = HttpContext.Current.Request;
            return (request["X-Requested-With"] == "XMLHttpRequest") || ((request.Headers != null) && (request.Headers["X-Requested-With"] == "XMLHttpRequest"));
        }

		public static void WriteResponseXml(string xml, string fileName)
		{
			if (HttpContext.Current != null && !Converter.IsEmpty(xml))
			{
				XmlDocument document = new XmlDocument();
				document.LoadXml(xml);
				XmlDeclaration decl = document.FirstChild as XmlDeclaration;
				if (decl != null)
					decl.Encoding = "utf-8";

				HttpResponse response = HttpContext.Current.Response;
				response.Clear();
				response.Charset = "utf-8";
				response.ContentType = "text/xml";
				response.AddHeader("content-disposition", "attachment; filename=" + fileName);
				response.BinaryWrite(Encoding.UTF8.GetBytes(document.InnerXml));
				response.End();
			}
		}

		public static void WriteResponseTxt(string txt, string fileName)
		{
			if (HttpContext.Current != null && !Converter.IsEmpty(txt))
			{
				HttpResponse response = HttpContext.Current.Response;
				response.Clear();
				response.Charset = "utf-8";
				response.ContentType = "text/plain";
				response.AddHeader("content-disposition", "attachment; filename=" + fileName);
				response.BinaryWrite(Encoding.UTF8.GetBytes(txt));
				response.End();
			}
		}

		public static void WriteResponseXls(string filePath, string targetFileName)
		{
			if (HttpContext.Current != null && !Converter.IsEmpty(filePath))
			{
				HttpResponse response = HttpContext.Current.Response;
				response.Clear();
				response.Charset = "utf-8";
				response.ContentType = "text/xls";
				response.AddHeader("content-disposition", "attachment; filename=" + targetFileName);
				response.BinaryWrite(File.ReadAllBytes(filePath));
				response.End();
			}
		}

		public static void WriteResponsePdf(string filePath, string targetFileName)
		{
			if (HttpContext.Current != null && !Converter.IsEmpty(filePath))
			{
				HttpResponse response = HttpContext.Current.Response;
				response.Clear();
				response.Charset = "utf-8";
				response.ContentType = "text/pdf";
				response.AddHeader("content-disposition", "attachment; filename=" + targetFileName);
				response.BinaryWrite(File.ReadAllBytes(filePath));
				response.End();
			}
		}

		public static string FLabel(string id, int width, string text, bool showTitle, string title, string styles, string onClick)
		{
			string[] r = new string[7];

			r[0] = "<div";

			if (!Converter.IsEmpty(id))
				r[1] = " id=\"" + id + "\"";
			if (showTitle && (!Converter.IsEmpty(title) || !Converter.IsEmpty(text)))
				r[2] = " title=\"" + (Converter.IsEmpty(title) ? text : title).Replace("\"", "&quot;") + "\"";
			if (Converter.IsEmpty(styles))
				r[3] = " style=\"width: " + width + "px\"";
			else
				r[3] = " style=\"width: " + width + "px; " + styles + "\"";

			r[4] = " class=\"fixLength\"";

			if (!Converter.IsEmpty(onClick))
				r[5] = " onclick=\"" + onClick + "\"";

			r[6] = ">" + text + "</div>";

			return string.Join(string.Empty, r);
		}

		#endregion
	}
}