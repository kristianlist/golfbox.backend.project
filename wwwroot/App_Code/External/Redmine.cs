﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Externel.Redmine
{    
    public class Projects
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Identifier { get; set; }
        public string Description { get; set; }
        public Parent Parent { get; set; }
        public DateTime Created_on { get; set; }
        public DateTime Updated_on { get; set; }

        public Projects()
        {
            //
            // TODO: Add constructor logic here
            //
        }
    }

    public class Parent
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}


