namespace WebSite.Common.Locale
{
	using System;
	using System.Threading;
	using System.Web;

	public partial class ClientCulture
	{
		#region Properties

		public string UICulture
		{
			get
			{
				string[] tmp = this.LanguageCode.ToString("F").Split(new char[] { '_' });

				return tmp[0].ToLower() + "-" + tmp[1];
			}
		}

		public LanguageCodeEnum LanguageCode { get; set; }

		#endregion

		#region Ctor

		public ClientCulture()
		{
		}

		#endregion

		#region Methods

		public void Save()
		{
			SetCookie("ClientCulture", this.UICulture);
		}

		public void Load()
		{
			LanguageCodeEnum tmpCode;
			string cookie = GetCookie("ClientCulture");

			if (!Converter.IsEmpty(cookie) && Enum.TryParse<LanguageCodeEnum>(cookie.Replace("-", "_"), true, out tmpCode))
			{
				this.LanguageCode = tmpCode;
				return;
			}

			/*string cultureName = Thread.CurrentThread.CurrentCulture.Name;

			if (!Converter.IsEmpty(cultureName) && Enum.TryParse<LanguageCodeEnum>(cultureName.Replace("-", "_"), true, out tmpCode))
			{
				this.LanguageCode = tmpCode;
				return;
			}*/

			this.LanguageCode = LanguageCodeEnum.EN_GB;
		}

		public void LoadByLCID(int lcid)
		{
			this.LanguageCode = (LanguageCodeEnum)lcid;
		}

		#endregion

		#region Utilities

		private static void SetCookie(string name, string value)
		{
			if (HttpContext.Current != null)
			{
				HttpCookie cookie = new HttpCookie(name);
				cookie.Value = HttpContext.Current.Server.UrlEncode(value);
				HttpContext.Current.Response.Cookies.Add(cookie);
			}
		}

		private static string GetCookie(string name)
		{
			if (HttpContext.Current != null && HttpContext.Current.Request.Cookies[name] != null)
			{
				string tmp = HttpContext.Current.Request.Cookies[name].Value;
				return HttpContext.Current.Server.UrlDecode(tmp);
			}
			return string.Empty;
		}

		#endregion
	}
}