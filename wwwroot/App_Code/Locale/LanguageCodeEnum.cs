namespace WebSite.Common.Locale
{
	public enum LanguageCodeEnum
	{
		/// <summary>
		/// 1030 | Danish - Denmark
		/// </summary>
		DA_DK = 1030,

		/// <summary>
		/// 1031 | German - Germany
		/// </summary>
		DE_DE = 1031,

		/// <summary>
		/// 1033 | English - United States
		/// </summary>
		EN_US = 1033,

		/// <summary>
		/// 1034 | Spanish - Spain
		/// </summary>
		ES_ES = 1034,

		/// <summary>
		/// 1035 | Finnish - Finland
		/// </summary>
		FI_FI = 1035,

		/// <summary>
		/// 1044 | Norwegian - Norway
		/// </summary>
		NB_NO = 1044,

		/// <summary>
		/// 1053 | Swedish - Sweden
		/// </summary>
		SV_SE = 1053,

		/// <summary>
		/// 1061 | Estonian - Estonia
		/// </summary>
		ET_EE = 1061,

		/// <summary>
		/// 2052 | Chinese - China
		/// </summary>
		ZH_CN = 2052,

		/// <summary>
		/// 2057 | English - Great Britain
		/// </summary>
		EN_GB = 2057,

        /// <summary>
        /// 3081 | English - Australia
        /// </summary>
        EN_AU = 3081,
        
        /// <summary>
		/// 3084 | French - Canada
		/// </summary>
		FR_CA = 3084,

		/// <summary>
		/// 4105 | English - Canada
		/// </summary>
		EN_CA = 4105
	}
}