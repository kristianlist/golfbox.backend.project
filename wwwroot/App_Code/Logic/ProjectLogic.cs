﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GolfBox.RM.Entities.Project;
using GolfBox.RM.Data.Project;
using GolfBox.RM.Exceptions;

namespace GolfBox.RM.Logic.Project
{
    public class ProjectLogic
    {
        #region Insert / Update / Delete

        /// <summary>
        /// This method inserts a ProjectData object into the DB It returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static ProjectData Insert(ProjectData indata)
        {
            Validate(indata);
            ProjectData outdata = Insert(indata);

            return outdata;
        }

        /// <summary>
        /// This method inserts a ProjectData list object into the DB It returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static List<ProjectData> Insert(List<ProjectData> indataList)
        {
            Validate(indataList);
            List<ProjectData> outDatalist = new List<ProjectData>();
            if (indataList.Count > 0)
            {
                foreach (ProjectData project in indataList)
                {
                    outDatalist.Add(Insert(project));
                }
            }

            return outDatalist;
        }

        /// <summary>
        /// This method updates a ProjectData object in the DB It returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static ProjectData Update(ProjectData indata)
        {
            Validate(indata);

            ProjectData outData = Update(indata);

            return outData;
        }

        /// <summary>
        /// This method updates a ProjectData object in the DB It returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static List<ProjectData> Update(List<ProjectData> indataList)
        {
            Validate(indataList);
            List<ProjectData> outDataList = new List<ProjectData>();
            if (indataList.Count > 0)
            {
                foreach (ProjectData project in indataList)
                {
                    outDataList.Add(Update(project));
                }
            }
            return outDataList;
        }

        /// <summary>
        /// This method deletes a ProjectData object in the DB It returns the deleted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static ProjectData Delete(int projectRID)
        {
            ProjectData deleted = ProjectDalc.Delete(projectRID);

            return deleted;
        }

        #endregion

        #region Load / Get

        /// <summary>
        /// This method loads a ProjectData object from the DB It returns the loaded object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static ProjectData Load(int projectRID)
        {
            ProjectData outData = ProjectDalc.Load(projectRID);

            return outData;
        }

        #endregion

        #region Validation

        /// <summary>
        /// This method validates a ProjectData object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        private static void Validate(ProjectData indata)
        {
            if (indata == null)
            {
                throw new ValidationException("Result cannot be validated");
            }
            if (indata.ID == 0)
            {
                throw new ValidationException(String.Format("Invalid project - The project reference is not valid"));
            }
        }

        /// <summary>
        /// This method validates a list of ProjectData objects
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        private static void Validate(List<ProjectData> indataList)
        {
            if (indataList.Count > 0)
            {
                foreach (ProjectData project in indataList)
                {
                    Validate(project);
                }
            }
        }

        #endregion

        #region Utility

        public static void UpdateFromExternal(out int ProjectsUpdated)
        {
            ProjectsUpdated = 0;
            List<ProjectData> Projects = ProjectLogic.Lists.All();

            DateTime LatestUpdate = (Projects.Count > 0) ? Projects.Max(x => x.UpdatedOn).Value : new DateTime(1900, 12, 31);

            External.ProjectsJSON ExternalProjects = External.GetProjects();

            foreach (External.ExtendedProject ExternalProject in ExternalProjects.projects)
            {
                if (ExternalProject.updated_on.DateTime > LatestUpdate)
                {
                    ProjectsUpdated += 1;
                    ProjectData Project = new ProjectData();
                    Project.ID = ExternalProject.id;
                    Project.Name = ExternalProject.name;
                    Project.Identifier = ExternalProject.identifier;
                    Project.Description = ExternalProject.description;
                    if (ExternalProject.parent != null)
                        Project.Parent_ID = ExternalProject.parent.id;
                    Project.CreatedOn = ExternalProject.created_on.DateTime;
                    Project.UpdatedOn = ExternalProject.updated_on.DateTime;

                    if (Projects.Count > 0 && Projects.Exists(x => x.ID == Project.ID))
                    {
                        ProjectDalc.Update(Project);
                    }
                    else
                    {
                        ProjectDalc.Insert(Project);
                    }
                }
            }
        }

        #endregion

        #region Lists

        public static class Lists
        {
            /// <summary>
            /// This Method gets a list of ProjectData objects
            /// </summary>
            /// <author>Sune Bruun Olsen</author>
            /// <created>18-12-2015</created>
            public static List<ProjectData> All()
            {
                List<ProjectData> projects = ProjectDalc.Lists.All();

                return projects;
            }
        }

        #endregion
    }
}