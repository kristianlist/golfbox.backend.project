﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GolfBox.RM.Exceptions;
using GolfBox.RM.Entities.TimeEntry;
using GolfBox.RM.Data.TimeEntry;

namespace GolfBox.RM.Logic.TimeEntry
{
    public class TimeEntryLogic
    {
        #region Insert / Update / Delete

        /// <summary>
        /// This method inserts a TimeEntryData object into the DB It returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static TimeEntryData Insert(TimeEntryData indata)
        {
            Validate(indata);
            TimeEntryData outdata = Insert(indata);

            return outdata;
        }

        /// <summary>
        /// This method inserts a TimeEntryData list object into the DB It returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static List<TimeEntryData> Insert(List<TimeEntryData> indataList)
        {
            Validate(indataList);
            List<TimeEntryData> outDatalist = new List<TimeEntryData>();
            if (indataList.Count > 0)
            {
                foreach (TimeEntryData timeEntry in indataList)
                {
                    outDatalist.Add(Insert(timeEntry));
                }
            }

            return outDatalist;
        }

        /// <summary>
        /// This method updates a TimeEntryData object in the DB It returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static TimeEntryData Update(TimeEntryData indata)
        {
            Validate(indata);

            TimeEntryData outData = Update(indata);

            return outData;
        }

        /// <summary>
        /// This method updates a TimeEntryData object in the DB It returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static List<TimeEntryData> Update(List<TimeEntryData> indataList)
        {
            Validate(indataList);
            List<TimeEntryData> outDataList = new List<TimeEntryData>();
            if (indataList.Count > 0)
            {
                foreach (TimeEntryData timeEntry in indataList)
                {
                    outDataList.Add(Update(timeEntry));
                }
            }
            return outDataList;
        }

        /// <summary>
        /// This method deletes a TimeEntryData object in the DB It returns the deleted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static TimeEntryData Delete(int timeEntryID)
        {
            TimeEntryData deleted = TimeEntryDalc.Delete(timeEntryID);

            return deleted;
        }

        #endregion

        #region Load / Get

        /// <summary>
        /// This method loads a TimeEntryData object from the DB It returns the loaded object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static TimeEntryData Load(int timeEntryID)
        {
            TimeEntryData outData = TimeEntryDalc.Load(timeEntryID);

            return outData;
        }

        #endregion

        #region Validation

        /// <summary>
        /// This method validates a TimeEntryData object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        private static void Validate(TimeEntryData indata)
        {
            if (indata == null)
            {
                throw new ValidationException("Result cannot be validated");
            }
            if (indata.ID == 0)
            {
                throw new ValidationException(String.Format("Invalid TimeEntry - The TimeEntry reference is not valid"));
            }
        }

        /// <summary>
        /// This method validates a list of TimeEntryData objects
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        private static void Validate(List<TimeEntryData> indataList)
        {
            if (indataList.Count > 0)
            {
                foreach (TimeEntryData timeEntry in indataList)
                {
                    Validate(timeEntry);
                }
            }
        }

        #endregion

        #region Utility

        public static void UpdateFromExternal(out int TimeEntriesUpdated)
        {
            TimeEntriesUpdated = 0;
            List<TimeEntryData> TimeEntries = TimeEntryLogic.Lists.All();

            DateTime LatestUpdate = (TimeEntries.Count > 0) ? TimeEntries.Max(x => x.UpdatedOn).Value : new DateTime(1900, 12, 31);

            External.TimeEntriesJSON ExternalTimeEntries = External.GetTimeEntries();

            int totalCount = ExternalTimeEntries.total_count;
            int currentOffset = ExternalTimeEntries.time_entries.Count();

            // sorting does not work correctly in the Redmine API, so we go two months back based on "spent on"
            while (totalCount > currentOffset && DateTime.Parse(ExternalTimeEntries.time_entries.Last().spent_on) > LatestUpdate.AddMonths(-2))
            {
                External.TimeEntriesJSON ExtraExternalTimeEntries = External.GetTimeEntries(currentOffset);
                ExternalTimeEntries.time_entries.AddRange(ExtraExternalTimeEntries.time_entries);
                currentOffset = ExternalTimeEntries.time_entries.Count();
            }

            foreach (External.TimeEntry ExternalTimeEntry in ExternalTimeEntries.time_entries)
            {
                if (ExternalTimeEntry.updated_on.DateTime > LatestUpdate)
                {
                    TimeEntriesUpdated += 1;
                    TimeEntryData TimeEntry = new TimeEntryData();
                    TimeEntry.ID = ExternalTimeEntry.id;
                    TimeEntry.Hours = (ExternalTimeEntry.hours*10000);
                    TimeEntry.SpentOn = DateTime.Parse(ExternalTimeEntry.spent_on);
                    TimeEntry.Comment = ExternalTimeEntry.comments;
                    TimeEntry.ProjectID = ExternalTimeEntry.project.id;
                    TimeEntry.IssueID = (ExternalTimeEntry.issue == null ? 0 : ExternalTimeEntry.issue.id);
                    TimeEntry.UserID = ExternalTimeEntry.user.id;
                    TimeEntry.ActivityID = ExternalTimeEntry.activity.id;
                    TimeEntry.CreatedOn = ExternalTimeEntry.created_on.DateTime;
                    TimeEntry.UpdatedOn = ExternalTimeEntry.updated_on.DateTime;

                    TimeEntryDalc.Save(TimeEntry);
                }
            }
        }

        #endregion

        #region Lists

        public static class Lists
        {
            /// <summary>
            /// This Method gets a list of TimeEntryData objects
            /// </summary>
            /// <author>Sune Bruun Olsen</author>
            /// <created>18-12-2015</created>
            public static List<TimeEntryData> All()
            {
                List<TimeEntryData> timeEntries = TimeEntryDalc.Lists.All();

                return timeEntries;
            }

            /// <summary>
            /// This Method gets a list of TimeEntryData objects by IssueIDs
            /// </summary>
            /// <author>Peter Frost</author>
            /// <created>08-09-2016</created>
            public static List<TimeEntryData> ByIssueList(List<int> issueIdList, bool minimizeLoad = false)
            {
                List<TimeEntryData> timeEntries = TimeEntryDalc.Lists.ByIssueList(issueIdList, minimizeLoad);

                return timeEntries;
            }

            /// <summary>
            /// This Method gets a list of TimeEntryData objects by version on open issues
            /// </summary>
            /// <author>Peter Frost</author>
            /// <created>08-09-2016</created>
            public static List<TimeEntryData> ByVersionAndOpen(GolfBox.RM.Entities.Enums.Version versionID, bool minimizeLoad = false) {
                List<TimeEntryData> timeEntries = TimeEntryDalc.Lists.ByVersionAndOpen((int)versionID, minimizeLoad);

                return timeEntries;
            }

            /// <summary>
            /// This Method gets a list of TimeEntryData objects by IssueIDs
            /// </summary>
            /// <author>Peter Frost</author>
            /// <created>08-09-2016</created>
            public static List<TimeEntryData> ByUserList(List<int> userIdList)
            {
                List<TimeEntryData> timeEntries = TimeEntryDalc.Lists.ByUserList(userIdList);

                return timeEntries;
            }
        }

        #endregion
    }
}