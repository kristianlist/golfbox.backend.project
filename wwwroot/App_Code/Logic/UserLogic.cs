﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GolfBox.RM.Exceptions;
using GolfBox.RM.Entities.User;
using GolfBox.RM.Data.User;

namespace GolfBox.RM.Logic.User
{
    public class UserLogic
    {
        #region Insert / Update / Delete

        /// <summary>
        /// This method inserts a UserData object into the DB It returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static UserData Insert(UserData indata)
        {
            Validate(indata);
            UserData outdata = Insert(indata);

            return outdata;
        }

        /// <summary>
        /// This method inserts a UserData list object into the DB It returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static List<UserData> Insert(List<UserData> indataList)
        {
            Validate(indataList);
            List<UserData> outDatalist = new List<UserData>();
            if (indataList.Count > 0)
            {
                foreach (UserData user in indataList)
                {
                    outDatalist.Add(Insert(user));
                }
            }

            return outDatalist;
        }

        /// <summary>
        /// This method updates a UserData object in the DB It returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static UserData Update(UserData indata)
        {
            Validate(indata);

            UserData outData = Update(indata);

            return outData;
        }

        /// <summary>
        /// This method updates a UserData object in the DB It returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static List<UserData> Update(List<UserData> indataList)
        {
            Validate(indataList);
            List<UserData> outDataList = new List<UserData>();
            if (indataList.Count > 0)
            {
                foreach (UserData user in indataList)
                {
                    outDataList.Add(Update(user));
                }
            }
            return outDataList;
        }

        /// <summary>
        /// This method deletes a UserData object in the DB It returns the deleted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static UserData Delete(int userID)
        {
            UserData deleted = UserDalc.Delete(userID);

            return deleted;
        }

        #endregion

        #region Load / Get

        /// <summary>
        /// This method loads a UserData object from the DB It returns the loaded object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static UserData Load(int userID)
        {
            UserData outData = UserDalc.Load(userID);

            return outData;
        }

        #endregion

        #region Validation

        /// <summary>
        /// This method validates a UserData object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        private static void Validate(UserData indata)
        {
            if (indata == null)
            {
                throw new ValidationException("Result cannot be validated");
            }
            if (indata.ID == 0)
            {
                throw new ValidationException(String.Format("Invalid user - The user reference is not valid"));
            }
        }

        /// <summary>
        /// This method validates a list of UserData objects
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        private static void Validate(List<UserData> indataList)
        {
            if (indataList.Count > 0)
            {
                foreach (UserData user in indataList)
                {
                    Validate(user);
                }
            }
        }

        #endregion

        #region Utility

        public static void UpdateFromExternal(out int UsersUpdated)
        {
            UsersUpdated = 0;
            List<UserData> Users = UserLogic.Lists.All();

            External.UsersJSON ExternalUsers = External.GetUsers();

            foreach (External.ExtendedUser ExternalUser in ExternalUsers.users)
            {
                UsersUpdated += 1;

                UserData User = new UserData();
                User.ID = ExternalUser.id;
                User.Login = ExternalUser.login;
                User.Firstname = ExternalUser.firstname;
                User.Lastname = ExternalUser.lastname;
                User.Email = ExternalUser.mail;

                if (Users.Count > 0 && Users.Exists(x => x.ID == User.ID))
                {
                    UserDalc.Update(User);
                }
                else
                {
                    UserDalc.Insert(User);
                }
            }
        }

        #endregion

        #region Lists

        public static class Lists
        {
            /// <summary>
            /// This Method gets a list of UserData objects
            /// </summary>
            /// <author>Sune Bruun Olsen</author>
            /// <created>18-12-2015</created>
            public static List<UserData> All()
            {
                List<UserData> users = UserDalc.Lists.All();

                return users;
            }
        }

        #endregion
    }
}