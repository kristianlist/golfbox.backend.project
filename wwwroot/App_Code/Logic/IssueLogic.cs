﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GolfBox.RM.Exceptions;
using GolfBox.RM.Entities.Issue;
using GolfBox.RM.Data.Issue;
using GolfBox.RM.Entities.Enums;

namespace GolfBox.RM.Logic.Issue
{
    public class IssueLogic
    {
        #region Insert / Update / Delete

        /// <summary>
        /// This method inserts a IssueData object into the DB It returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static IssueData Insert(IssueData indata)
        {
            Validate(indata);
            IssueData outdata = Insert(indata);

            return outdata;
        }

        /// <summary>
        /// This method inserts a IssueData list object into the DB It returns the inserted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static List<IssueData> Insert(List<IssueData> indataList)
        {
            Validate(indataList);
            List<IssueData> outDatalist = new List<IssueData>();
            if (indataList.Count > 0)
            {
                foreach (IssueData issue in indataList)
                {
                    outDatalist.Add(Insert(issue));
                }
            }

            return outDatalist;
        }

        /// <summary>
        /// This method updates a IssueData object in the DB It returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static IssueData Update(IssueData indata)
        {
            Validate(indata);

            IssueData outData = Update(indata);

            return outData;
        }

        /// <summary>
        /// This method updates a IssueData list object in the DB It returns the updated object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static List<IssueData> Update(List<IssueData> indataList)
        {
            Validate(indataList);
            List<IssueData> outDataList = new List<IssueData>();
            if (indataList.Count > 0)
            {
                foreach (IssueData project in indataList)
                {
                    outDataList.Add(Update(project));
                }
            }
            return outDataList;
        }

        /// <summary>
        /// This method deletes a IssueData object in the DB It returns the deleted object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static IssueData Delete(int issueID)
        {
            IssueData deleted = IssueDalc.Delete(issueID);

            return deleted;
        }

        #endregion

        #region Load / Get

        /// <summary>
        /// This method loads a IssueData object from the DB It returns the loaded object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        public static IssueData Load(int issueID)
        {
            IssueData outData = IssueDalc.Load(issueID);

            return outData;
        }

        #endregion

        #region Validation

        /// <summary>
        /// This method validates a IssueData object
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        private static void Validate(IssueData indata)
        {
            if (indata == null)
            {
                throw new ValidationException("Result cannot be validated");
            }
            if (indata.ID == 0)
            {
                throw new ValidationException(String.Format("Invalid Issue - The Issue reference is not valid"));
            }
        }

        /// <summary>
        /// This method validates a list of IssueData objects
        /// </summary>
        /// <author>Sune Bruun Olsen</author>
        /// <created>18-12-2015</created>
        private static void Validate(List<IssueData> indataList)
        {
            if (indataList.Count > 0)
            {
                foreach (IssueData issue in indataList)
                {
                    Validate(issue);
                }
            }
        }

        #endregion

        #region Utility

        public static List<int> StatsFromExternal()
        {
            List<int> statsList = new List<int>();

            External.IssuesJSON queryIssues1 = External.GetQuery(391); //Statistik 1 - Prio over 20
            int stats1 = queryIssues1.total_count;
            External.IssuesJSON queryIssues2 = External.GetQuery(392); //Statistik 2 - Prio under 20, importance over 4,5
            int stats2 = queryIssues2.total_count;
            External.IssuesJSON queryIssues3 = External.GetQuery(390); //Statistik 3 - Mangler prio
            int stats3 = queryIssues3.total_count;
            External.IssuesJSON queryIssues4 = External.GetQuery(393); //Statistik 4 - Backlog Total
            int stats4 = queryIssues4.total_count;
            External.IssuesJSON queryIssues5 = External.GetQuery(394); //Statistik 5 - Kandidater Total
            int stats5 = queryIssues5.total_count;
            External.IssuesJSON queryIssues6 = External.GetQuery(395); //Statistik 6 - Igangværende Total
            int stats6 = queryIssues6.total_count;

            statsList.Add(stats1);
            statsList.Add(stats2);
            statsList.Add(stats3);
            statsList.Add(stats4 - stats1 - stats2 - stats3);
            statsList.Add(stats4);
            statsList.Add(stats5);
            statsList.Add(stats6);
            statsList.Add(stats4 + stats5 + stats6);

            return statsList;
        }

        public static void UpdateFromExternal(out int IssuesUpdated)
        {
            IssuesUpdated = 0;
            List<IssueData> Issues = IssueLogic.Lists.All();

            DateTime LatestUpdate = (Issues.Count > 0) ? Issues.Max(x => x.UpdatedOn).Value : new DateTime(1900, 12, 31);

            External.IssuesJSON ExternalIssues = External.GetIssues();

            int totalCount = ExternalIssues.total_count;
            int currentOffset = ExternalIssues.issues.Count();

            while (totalCount > currentOffset && ExternalIssues.issues.Last().updated_on.DateTime > LatestUpdate)
            {
                External.IssuesJSON ExtraExternalIssues = External.GetIssues(currentOffset);
                ExternalIssues.issues.AddRange(ExtraExternalIssues.issues);
                currentOffset = ExternalIssues.issues.Count();
            }

            foreach (External.ExtendedIssue ExternalIssue in ExternalIssues.issues)
            {
                if (ExternalIssue.updated_on.DateTime > LatestUpdate)
                {
                    IssuesUpdated += 1;
                    IssueData Issue = new IssueData();
                    Issue.ID = ExternalIssue.id;
                    if (ExternalIssue.parent != null)
                        Issue.ParentID = ExternalIssue.parent.id;
                    Issue.Subject = ExternalIssue.subject;
                    Issue.Description = ExternalIssue.description;
                    Issue.EstimatedHours = (ExternalIssue.estimated_hours*10000);
                    External.CustomField customFieldEstimatedRisk = (ExternalIssue.custom_fields != null ? ExternalIssue.custom_fields.Find(x => x.id == 15) : null);
                    if (customFieldEstimatedRisk != null)
                    {
                        int risk = 0;
                        if (int.TryParse(customFieldEstimatedRisk.value, out risk))
                            Issue.EstimatedRisk = risk;
                    }
                    Issue.DoneRatio = ExternalIssue.done_ratio;
                    Issue.StatusID = ExternalIssue.status.id;
                    Issue.ProjectID = ExternalIssue.project.id;
                    Issue.TrackerID = ExternalIssue.tracker.id;
                    Issue.PriorityID = ExternalIssue.priority.id;
                    Issue.AuthorID = ExternalIssue.author.id;
                    if (ExternalIssue.assigned_to != null)
                        Issue.AssignedToID = ExternalIssue.assigned_to.id;
                    if (ExternalIssue.fixed_version != null)
                        Issue.FixedVersionID = ExternalIssue.fixed_version.id;
                    if (!String.IsNullOrEmpty(ExternalIssue.due_date))
                    {
                        DateTime duedate;
                        if (DateTime.TryParse(ExternalIssue.due_date, out duedate))
                            Issue.DueDate = duedate;
                    }
                    //External.CustomField customFieldNextGroomingDate = (ExternalIssue.custom_fields != null ? ExternalIssue.custom_fields.Find(x => x.id == 13) : null);
                    //if (customFieldNextGroomingDate != null)
                    //{
                    //    DateTime grooming;
                    //    if (DateTime.TryParse(customFieldNextGroomingDate.value, out grooming))
                    //        Issue.NextGroomingDate = grooming;
                    //}
                    Issue.CreatedOn = ExternalIssue.created_on.DateTime;
                    Issue.UpdatedOn = ExternalIssue.updated_on.DateTime;

                    IssueDalc.Save(Issue);
                }
            }
        }

        #endregion

        #region Lists

        public static class Lists
        {
            /// <summary>
            /// This Method gets a list of IssueData objects
            /// </summary>
            /// <author>Sune Bruun Olsen</author>
            /// <created>18-12-2015</created>
            public static List<IssueData> All()
            {
                List<IssueData> issues = IssueDalc.Lists.All();

                return issues;
            }


            /// <summary>
            /// This Method gets a list of IssueData objects by Version
            /// </summary>
            /// <author>Peter Frost</author>
            /// <created>06-09-2016</created>
            public static List<IssueData> ByVersion(GolfBox.RM.Entities.Enums.Version versionID, bool minimizeLoad = false)
            {
                List<IssueData> issues = IssueDalc.Lists.ByVersion((int)versionID, minimizeLoad);

                return issues;
            }

            /// <summary>
            /// This Method gets a list of IssueData objects by Version
            /// </summary>
            /// <author>Kristian List</author>
            /// <created>13-05-2020</created>
            public static List<IssueData> ByVersionAndOpen(GolfBox.RM.Entities.Enums.Version versionID, bool minimizeLoad = false) {
                List<IssueData> issues = IssueDalc.Lists.ByVersionAndOpen((int)versionID, minimizeLoad);

                return issues;
            }

            /// <summary>
            /// This Method gets a list of IssueData objects by Assignee
            /// </summary>
            /// <author>Peter Frost</author>
            /// <created>09-09-2016</created>
            public static List<IssueData> ByAssigneeList(List<int> userIdList)
            {
                List<IssueData> issues = IssueDalc.Lists.ByAssigneeList(userIdList);

                return issues;
            }
        }

        #endregion
    }
}