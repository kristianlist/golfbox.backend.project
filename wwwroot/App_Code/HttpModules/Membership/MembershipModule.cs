using System.Collections.Generic;
using System.Linq;

namespace WebSite.HttpModules.Membership {
    using System;
    using System.Web;
    using System.Web.SessionState;

    public class MembershipModule : IHttpModule, IRequiresSessionState {
        public void Init( HttpApplication context ) {
            context.AuthenticateRequest += new EventHandler(OnAuthenticateRequest);
            context.AuthorizeRequest += new EventHandler(OnAuthorizeRequest);
            context.PostAcquireRequestState += new EventHandler(OnPostAcquireRequestState);
        }

        public void Dispose() {
        }

        private void OnPostAcquireRequestState( object sender, EventArgs e ) {
            HttpApplication app = sender as HttpApplication;

            if(app == null || app.Context == null)
                return;
        }

        private void OnAuthenticateRequest( object sender, EventArgs e ) {
            HttpApplication app = sender as HttpApplication;

            if(app == null || app.Context == null)
                return;

            app.Context.User = System.Threading.Thread.CurrentPrincipal;
        }


        private void OnAuthorizeRequest( object sender, EventArgs e ) {
            HttpApplication app = sender as HttpApplication;

            if(app == null || app.Context == null)
                return;
        }
    }
}