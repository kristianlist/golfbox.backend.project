namespace WebSite.HttpModules.Error
{
	using System;
	using System.Web;

	public class ErrorModule : IHttpModule
	{
		public void Init(HttpApplication context)
		{
			context.Error += new EventHandler(OnError);
		}

		public void Dispose()
		{
		}

		private void OnError(object sender, EventArgs e)
		{
			HttpApplication app = sender as HttpApplication;

			if (app == null || app.Context == null)
				return;

			string ext = app.Request.AppRelativeCurrentExecutionFilePath.ToLower();

			if (!(ext.EndsWith(".aspx") || ext.EndsWith(".asmx") || ext.EndsWith(".ashx")))
				return;

			Exception lastException = app.Server.GetLastError();

			try
			{
                //log error somehow
			}
			catch { }
		}
	}
}