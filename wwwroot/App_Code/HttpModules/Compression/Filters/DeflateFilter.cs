namespace WebSite.HttpModules.Compression
{
	using System.IO;
	using System.IO.Compression;

	public class DeflateFilter : CompressingFilter
	{
		private readonly DeflateStream _stream;

		public DeflateFilter(Stream baseStream)
			: base(baseStream)
		{
			_stream = new DeflateStream(baseStream, CompressionMode.Compress);
		}

		public override string ContentEncoding
		{
			get
			{
				return "deflate";
			}
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			if (!HasWrittenHeaders)
				WriteHeaders();
			_stream.Write(buffer, offset, count);
		}

		public override void Close()
		{
			_stream.Close();
		}

		public override void Flush()
		{
			_stream.Flush();
		}
	}
}