namespace WebSite.HttpModules.Compression
{
	using System.IO;
	using System.Web;

	public abstract class CompressingFilter : HttpOutputFilter
	{
		private bool _HasWrittenHeaders;

		protected bool HasWrittenHeaders
		{
			get
			{
				return _HasWrittenHeaders;
			}
		}

		protected CompressingFilter(Stream baseStream)
			: base(baseStream)
		{
		}

		public abstract string ContentEncoding { get; }

		internal void WriteHeaders()
		{
			HttpContext.Current.Response.AppendHeader("Content-Encoding", ContentEncoding);
			HttpContext.Current.Response.AppendHeader("X-Compressed-By", "T2-Compression");
			_HasWrittenHeaders = true;
		}
	}
}