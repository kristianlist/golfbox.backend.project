namespace WebSite.HttpModules.Compression
{
	using System.IO;
	using System.IO.Compression;

	public class GZipFilter : CompressingFilter
	{
		private readonly GZipStream _stream;

		public GZipFilter(Stream baseStream)
			: base(baseStream)
		{
			_stream = new GZipStream(baseStream, CompressionMode.Compress);
		}

		public override string ContentEncoding
		{
			get
			{
				return "gzip";
			}
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			if (!HasWrittenHeaders)
				WriteHeaders();
			_stream.Write(buffer, offset, count);
		}

		public override void Close()
		{
			_stream.Close();
		}

		public override void Flush()
		{
			_stream.Flush();
		}
	}
}