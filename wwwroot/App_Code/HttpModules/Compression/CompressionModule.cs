namespace WebSite.HttpModules.Compression
{
	using System;
	using System.IO;
	using System.Web;

	public class CompressionModule : IHttpModule
	{
		public void Init(HttpApplication context)
		{
			context.ReleaseRequestState   += new EventHandler(CompressContent);
			context.PreSendRequestHeaders += new EventHandler(CompressContent);
			context.Error                 += new EventHandler(OnError);
		}

		public void Dispose()
		{
		}

		private void CompressContent(object sender, EventArgs e)
		{
			HttpApplication app = sender as HttpApplication;

			if (app == null || app.Context == null)
				return;

			bool isOutputCached = false;

			string isCached = app.Context.Items["T2OutputCache"] as string;

			if (!Converter.IsEmpty(isCached))
				isOutputCached = isCached == "1" ? true : false;

			if (!isOutputCached)
			{
				string ext = app.Request.AppRelativeCurrentExecutionFilePath.ToLower();

				if (!(ext.EndsWith(".aspx") || ext.EndsWith(".asmx") || ext.EndsWith(".ashx") || ext.EndsWith(".js") || ext.EndsWith(".css")))
					return;
			}

			if (app.Response.ContentType == null)
				return;

			if (!app.Context.Items.Contains("T2.Compressed"))
			{
				app.Context.Items.Add("T2.Compressed", true);

				string acceptedTypes = app.Request.Headers["Accept-Encoding"];

				if (acceptedTypes == null)
					return;

				app.Response.Cache.VaryByHeaders["Accept-Encoding"] = true;

				CompressingFilter filter = GetFilterForScheme(acceptedTypes.Split(','), app.Response.Filter);
				if(filter != null)
                    filter.WriteHeaders();

				if (!isOutputCached && filter != null)
					app.Response.Filter = filter;
			}
		}

		private void OnError(object sender, EventArgs e)
		{
			HttpApplication app = sender as HttpApplication;

			if (app == null || app.Context == null)
				return;

			string ext = app.Request.AppRelativeCurrentExecutionFilePath.ToLower();

			if (!(ext.EndsWith(".aspx") || ext.EndsWith(".asmx") || ext.EndsWith(".ashx") || ext.EndsWith(".js") || ext.EndsWith(".css")))
				return;

			app.Context.Items.Add("T2.Compressed", false);
		}

		private CompressingFilter GetFilterForScheme(string[] schemes, Stream output)
		{
			bool foundDeflate = false;
			bool foundGZip    = false;
			bool foundStar    = false;

			float deflateQuality = 0f;
			float gZipQuality    = 0f;
			float starQuality    = 0f;

			bool isAcceptableDeflate;
			bool isAcceptableGZip;
			bool isAcceptableStar;

			for (int i = 0; i <= schemes.Length - 1; i++)
			{
				string acceptEncodingValue = schemes[i].Trim().ToLower();

				if (acceptEncodingValue.StartsWith("deflate"))
				{
					foundDeflate = true;
					float newDeflateQuality = GetQuality(acceptEncodingValue);

					if (deflateQuality < newDeflateQuality)
						deflateQuality = newDeflateQuality;
				}
				else if (acceptEncodingValue.StartsWith("gzip") || acceptEncodingValue.StartsWith("x-gzip"))
				{
					foundGZip = true;
					float newGZipQuality = GetQuality(acceptEncodingValue);

					if (gZipQuality < newGZipQuality)
						gZipQuality = newGZipQuality;
				}
				else if (acceptEncodingValue.StartsWith("*"))
				{
					foundStar = true;
					float newStarQuality = GetQuality(acceptEncodingValue);

					if (starQuality < newStarQuality)
						starQuality = newStarQuality;
				}
			}

			isAcceptableStar    = foundStar && (starQuality > 0);
			isAcceptableDeflate = (foundDeflate && (deflateQuality > 0)) || (!foundDeflate && isAcceptableStar);
			isAcceptableGZip    = (foundGZip && (gZipQuality > 0)) || (!foundGZip && isAcceptableStar);

			if (isAcceptableDeflate && !foundDeflate)
				deflateQuality = starQuality;

			if (isAcceptableGZip && !foundGZip)
				gZipQuality = starQuality;

			if (!(isAcceptableDeflate || isAcceptableGZip || isAcceptableStar))
				return null;

			if (isAcceptableDeflate && (!isAcceptableGZip || deflateQuality > gZipQuality))
				return new DeflateFilter(output);

			if (isAcceptableGZip && (!isAcceptableDeflate || deflateQuality < gZipQuality))
				return new GZipFilter(output);

			if (isAcceptableGZip)
				return new GZipFilter(output);

			if (isAcceptableDeflate)
				return new DeflateFilter(output);

			if (isAcceptableDeflate || isAcceptableStar)
				return new DeflateFilter(output);

			if (isAcceptableGZip)
				return new GZipFilter(output);

			return null;
		}

		private float GetQuality(string acceptEncodingValue)
		{
			int qParam = acceptEncodingValue.IndexOf("q=");

			if (qParam >= 0)
			{
				float val;
				if (float.TryParse(acceptEncodingValue.Substring(qParam + 2, acceptEncodingValue.Length - (qParam + 2)), out val))
					return val;
				return 0f;
			}

			return 1f;
		}
	}
}