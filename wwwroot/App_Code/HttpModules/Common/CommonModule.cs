namespace WebSite.HttpModules.Common
{
	using System;
	using System.Web;

	public class CommonModule : IHttpModule
	{
		public void Init(HttpApplication context)
		{
			context.BeginRequest += new EventHandler(OnBeginRequest);
		}

		public void Dispose()
		{
		}

		private void OnBeginRequest(object sender, EventArgs e)
		{
			HttpApplication app = sender as HttpApplication;

			if (app == null || app.Context == null)
				return;

			string ext = app.Request.AppRelativeCurrentExecutionFilePath.ToLower();

			if (!(ext.EndsWith(".aspx") || ext.EndsWith(".asmx") || ext.EndsWith(".ashx")))
				return;

			WebSite.Common.Initialize.Init(app);

			// http://petesbloggerama.blogspot.com/2007/08/aspnet-loss-of-session-cookies-with.html
			app.Context.Response.AddHeader("P3P", "CP=\"IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA\"");

			app.Context.Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
			app.Context.Response.Cache.SetNoStore();
			app.Context.Response.Cache.SetExpires(DateTime.MinValue);
		}
	}
}