﻿let gantt = function () {
    let today = new Date(),
        map = Highcharts.map,
        series = [],
        developers;

    // Set to 00:00:00:000 today
    today.setUTCHours(0);
    today.setUTCMinutes(0);
    today.setUTCSeconds(0);
    today.setUTCMilliseconds(0);
    today = today.getTime();

    developers = {
        'PT': {
            name: "Poul Thomsen",
            short: "PT",
            allocatedTime: [
                {
                    project: 'GMS Player v2',
                    start: new Date(2021, 00, 04),
                    end: new Date(2021, 05, 31)
                }, {
                    project: 'NIF',
                    start: new Date(2021, 06, 01),
                    end: new Date(2021, 09, 30)
                }, {
                    project: 'GMS Admin v2',
                    start: new Date(2021, 10, 01),
                    end: new Date(2022, 08, 01)
                }
            ]
        },
        'RP': {
            name: "Ryan Pearse",
            short: "RP",
            allocatedTime: [
                {
                    project: 'Server',
                    start: new Date(),
                    end: new Date(2022, 11, 31)
                }
            ]
        },
        'KL': {
            name: "Kristian List",
            short: "KL",
            allocatedTime: [
                {
                    project: 'Konverteringstabeller',
                    start: new Date(2021, 00, 01),
                    end: new Date(2021, 00, 31)
                }, {
                    project: 'ProPlanner Vinter 2020',
                    start: new Date(2021, 01, 01),
                    end: new Date(2021, 01, 28)
                }
            ]
        },
        'CM': {
            name: "Christoffer Agger Mørch",
            short: "CM",
            allocatedTime: [
                {
                    project: 'LoyalTee (Hørsholm)',
                    start: new Date(),
                    end: new Date(2020, 11, 21)
                }, {
                    project: 'GMS Player v2',
                    start: new Date(2021, 00, 04),
                    end: new Date(2021, 03, 31)
                }, {
                    project: 'NIF',
                    start: new Date(2021, 06, 01),
                    end: new Date(2021, 09, 30)
                }, {
                    project: 'GMS Admin v2',
                    start: new Date(2021, 10, 01),
                    end: new Date(2022, 08, 01)
                }
            ]
        },
        'PS': {
            name: "Pete Surawatanapong",
            short: "PS",
            allocatedTime: [
                {
                    project: 'årsrapport',
                    start: new Date(),
                    end: new Date(2020, 11, 21)
                }, {
                    project: 'GMS Player v2',
                    start: new Date(2021, 00, 04),
                    end: new Date(2021, 01, 31)
                }, {
                    project: 'MinGolf',
                    start: new Date(2021, 02, 01),
                    end: new Date(2021, 03, 31)
                }
            ]
        },
        'SA': {
            name: "Steffen Andersen",
            short: "SA",
            allocatedTime: [
                {
                    project: 'LoyalTee (Hørsholm)',
                    start: new Date(),
                    end: new Date(2020, 11, 21)
                }, {
                    project: 'GMS Player v2',
                    start: new Date(2021, 00, 04),
                    end: new Date(2021, 05, 31)
                }, {
                    project: 'GMS Admin v2',
                    start: new Date(2021, 06, 01),
                    end: new Date(2022, 06, 01)
                }
            ]
        },
        'MT': {
            name: "Martin Thuesen",
            short: "MT",
            allocatedTime: [
                {
                    project: 'GMS techsupport',
                    start: new Date(),
                    end: new Date(2021, 01, 01)
                }, {
                    project: 'GMS Admin v2',
                    start: new Date(2021, 06, 01),
                    end: new Date(2022, 06, 01)
                }
            ]
        },
        'SD': {
            name: "Søren Didriksen",
            short: "SD",
            allocatedTime: [
                {
                    project: 'GMS Admin v2',
                    start: new Date(2021, 06, 01),
                    end: new Date(2022, 06, 01)
                }
            ]
        },
        'NB': {
            name: "Nikolaj Bille",
            short: "NB",
            allocatedTime: [
                {
                    project: 'WHS',
                    start: new Date(),
                    end: new Date(2021, 01, 28)
                }, {
                    project: 'Union DB for remaining countries',
                    start: new Date(2021, 02, 01),
                    end: new Date(2021, 10, 30)
                }
            ]
        },
        'ML': {
            name: "Mathias Lenz",
            short: "ML",
            allocatedTime: [
                {
                    project: 'Server',
                    start: new Date(),
                    end: new Date(2022, 11, 31)
                }
            ]
        },
        'RL': {
            name: "Rasmus Larsen",
            short: "RL",
            allocatedTime: [
                {
                    project: 'LoyalTee (Hørsholm)',
                    start: new Date(),
                    end: new Date(2020, 11, 21)
                }, {
                    project: 'GMS Player v2',
                    start: new Date(2021, 00, 04),
                    end: new Date(2021, 05, 31)
                }, {
                    project: 'GMS Admin v2',
                    start: new Date(2021, 06, 01),
                    end: new Date(2022, 06, 01)
                }
            ]
        },
        'KJ': {
            name: "Klaus Jørgensen",
            short: "KJ",
            allocatedTime: [
                {
                    project: 'WHS',
                    start: new Date(),
                    end: new Date(2020, 11, 15)
                }, {
                    project: 'DIBS easy',
                    start: new Date(2021, 00, 04),
                    end: new Date(2021, 00, 20)
                }, {
                    project: 'Player livescoring',
                    start: new Date(2021, 01, 01),
                    end: new Date(2021, 03, 30)
                }, {
                    project: 'MinGolf',
                    start: new Date(2021, 04, 01),
                    end: new Date(2021, 07, 31)
                }
            ]
        }        
    };

    for (const dev of Object.values(developers)) {
        let data = dev.allocatedTime
            .map((time) => {
                return {
                    id: "time_" + series.length,
                    name: time.project,
                    project: time.project,
                    start: time.start.getTime(),
                    end: time.end.getTime(),
                    y: series.length
                };
            })
            .sort((a, b) => {
                return a.start < b.start;
            });

        series[series.length] = {
            name: dev.name,
            data: data || null,
            current: data[0]
        };
    }

    Highcharts.ganttChart('gantt-container', {
        time: {
            useUTC: false
        },
        series: series,
        title: {
            text: 'Developer gantt'
        },
        tooltip: {
            pointFormat: '<span>Project: {point.project}</span><br/><span>Start: {point.start:%e. %b}</span><br/><span>End: {point.end:%e. %b}</span>'
        },
        xAxis: {
            currentDateIndicator: true
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '{point.name}',
                    style: {
                        textOutline: 'none'
                    }
                }
            }
        },
        yAxis: {
            type: 'category',
            grid: {
                columns: [{
                    title: {
                        text: 'Name'
                    },
                    categories: map(series, function (s) {
                        return s.name;
                    })
                }, {
                    title: {
                        text: 'Current project'
                    },
                    categories: map(series, function (s) {
                        return s.current ? s.current.project : '';
                    })
                }, {
                    title: {
                        text: 'Start'
                    },
                    categories: map(series, function (s) {
                        return s.current ? Highcharts.dateFormat('%e. %b', s.current.start) : '';
                    })
                }, {
                    title: {
                        text: 'End'
                    },
                    categories: map(series, function (s) {
                        return s.current ? Highcharts.dateFormat('%e. %b', s.current.end) : '';
                    })
                }]
            }
        }
    });
};