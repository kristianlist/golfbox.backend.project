﻿(function () {
	if (golfbox.ui === undefined)
		golfbox.ui = gb.ui = {};

	golfbox.ui.list = {};
	golfbox.ui.list.instances = {};
	golfbox.ui.list.create = function (target, properties) {
		var List = function (target, properties) {
			var list		= this;
			list.target		= target;
			list.dom		= $(list.target);
			list.columns	= properties.columns || [];
			list.items		= properties.items || [];
			list.id			= properties.id || ('list' + golfbox.getUniqueNumber());
			list.title		= properties.title || '';
			list.renderMax = properties.renderMax || 100000;

            var render = {};
            render.base = function(includeItems) { 
            	var output = [];

            	output.push('<div id="', list.id, '" class="list in-progress">');
            	output.push('<div class="header">');
            	output.push(list.title);
				output.push('</div>');
				output.push('<div class="columns">');

				for (var index = 0; index < list.columns.length; index++) {
					var column = list.columns[index];
					
					var align = column.textAlign == 'right' ? ' text-right' : '';

					output.push('<div class="column'+align+'" style="width: ', column.width || (Math.floor(100 / list.columns.length) + '%'), '">');
					
					if(column.title === undefined || column.title === null) {
						output.push(column.property);
					} else {
						output.push(column.title);
					}
					
					output.push('</div>');
				}

				output.push('</div>');
				output.push('<div class="items">');
				
				if(includeItems) {
					output.push(render.items());
				}

				output.push('</div>');
				output.push('<div class="footer">');
				output.push('</div>');
				output.push('</div>');

            	return output.join('');
			};

            render.items = function (start,end) { 
				var output = [];
				
				if(start === undefined) {
					start = 0;
				}

				if(end === undefined || end > list.items.length) {
					end = list.items.length;
				}

				for (var index = start; index < end; index++) {
					output.push(render.item(list.items[index]));
				}

				return output.join('');
			};

            render.item = function (item) { 
				var output = [];

				output.push('<div class="item">');
				
				for (var index = 0; index < list.columns.length; index++) {
					var column	= list.columns[index];
					var key		= column.property;
					var value	= undefined;

					if(item.hasOwnProperty(key)) {
						value = item[key];
					} else {
						var tmp = key.toLowerCase();
						if(tmp === key) {
							key = key[0].toUpperCase() + key.slice(1);
						} else {
							key = tmp;
						}

						if(item.hasOwnProperty(key)) {
							value = item[key];
						}
					}

					var align = column.textAlign == 'right' ? ' text-right' : '';
					
					output.push('<div class="column'+align+'" style="width: ', column.width || (Math.floor(100 / list.columns.length) + '%'), '">');
					if(value !== undefined && value !== null) {
					    value = column.format == 'decimal' ? parseFloat(value).toFixed(2) : value;
					    value = column.format == 'decimal' && value == '0.00' ? '-' : value;
					    output.push(value);
					}
					
					output.push('</div>');
				}

				output.push('</div>');

				return output.join('');
			};

            list.render = function () {
				if(list.items.length < list.renderMax) {
					list.dom.html(render.base(true));
				} else { 
					/* concept not working yet 
					 * gives same user experience
					 * as just doing it all in one go
					 * the idea was to split up
					 * the rendering into chunks
					 * to make it easier for the
					 * browser to continue working
					 * and we could display a spinner
					 * while list is being rendered
					 */
					list.dom.html(render.base());
					var $items = list.dom.find('.items');
					var current = 0;
					while (current < list.items.length) {
						$items.append(render.items(current, (current + list.renderMax))); 
						current = (current + list.renderMax);
            		}
				}

				list.dom.removeClass('in-progress');
			};
    	};

		var list = new List(target, properties || {});
        golfbox.ui.list.instances[list.id] = list;
        return list;
    };

})();
