/**
 * @preserve golfbox.lib-clientConverter.js
 */
(function($, window, undefined) {
	if (window.golfbox === undefined)
		window.golfbox = window.gb = {};

	var document  = window.document,
		navigator = window.navigator,
		location  = window.location,
		_         = window.golfbox;

	_.cc = _.clientConverter = {
		toTime: function(str) {
			var d = new Date(),
				r = str.split(':');

			r[0] && d.setHours(r[0]);
			r[1] && d.setMinutes(r[1]);
			r[2] && d.setSeconds(r[2]);
			r[3] && d.setMilliseconds(r[3]);

			return d;
		},

		getDateParts: function(str, lcid) {
			var x, y, yearMatrix, monthMatrix, dayMatrix, yyyy, mm, dd,
				lang = lcid || _.page.lcid;

			switch (lang) {
				case 3081:
				    x = !isNaN(str.substring(1, 2)) ? 2 : 1;
				    y = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
				    yearMatrix = [x + y + 2, x + y + 6];
				    monthMatrix = [x + 1, y + x + 1];
				    dayMatrix = [0, x];
				    break;

				case 1033:
					x           = !isNaN(str.substring(1, 2)) ? 2 : 1;
					y           = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
					yearMatrix  = [x + y + 2, x + y + 6];
					monthMatrix = [0, x];
					dayMatrix   = [x + 1, y + x + 1];
					break;

				case 1053:
					yearMatrix  = [0, 4];
					monthMatrix = [5, 7];
					dayMatrix   = [8, 10];
					break;

				case 1035:
					x           = !isNaN(str.substring(1, 2)) ? 2 : 1;
					y           = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
					yearMatrix  = [x + y + 2, x + y + 6];
					monthMatrix = [x + 1, y + x + 1];
					dayMatrix   = [0, x];
					break;

				case 1061:
					x           = !isNaN(str.substring(1, 2)) ? 2 : 1;
					y           = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
					yearMatrix  = [x + y + 2, x + y + 6];
					monthMatrix = [x + 1, y + x + 1];
					dayMatrix   = [0, x];
					break;

				case 1063:
					yearMatrix  = [0, 4];
					monthMatrix = [5, 7];
					dayMatrix   = [8, 10];
					break;

	            case 1031:
	                x = !isNaN(str.substring(1, 2)) ? 2 : 1;
	                y = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
	                yearMatrix = [x + y + 2, x + y + 6];
	                monthMatrix = [x + 1, y + x + 1];
	                dayMatrix = [0, x];
	                break;

	            case 2052:
					x           = !isNaN(str.substring(6, 7)) ? 2 : 1;
					y           = str.length == 8 + x ? 2 : 1;
					yearMatrix  = [0, 4];
					monthMatrix = [5, 5 + x];
					dayMatrix   = [5 + x + 1, str.length];
					break;

				case 2057:
					x           = !isNaN(str.substring(1, 2)) ? 2 : 1;
					y           = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
					yearMatrix  = [x + y + 2, x + y + 6];
					monthMatrix = [x + 1, y + x + 1];
					dayMatrix   = [0, x];
					break;

				default:
					yearMatrix  = [6, 10];
					monthMatrix = [3, 5];
					dayMatrix   = [0, 2];
			}

			yyyy = parseFloat(str.substring(yearMatrix[0], yearMatrix[1]));
			mm   = parseFloat(str.substring(monthMatrix[0], monthMatrix[1]));
			dd   = parseFloat(str.substring(dayMatrix[0], dayMatrix[1]));

			return [yyyy, mm, dd];
		},

		toDate: function(str, lcid) {
			var x, y, yearMatrix, monthMatrix, dayMatrix, yyyy, mm, dd,
				lang = lcid || _.page.lcid;

			switch (lang) {
				case 3081:
				    x = !isNaN(str.substring(1, 2)) ? 2 : 1;
				    y = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
				    yearMatrix = [x + y + 2, x + y + 6];
				    monthMatrix = [x + 1, y + x + 1];
				    dayMatrix = [0, x];
				    break;

				case 1033:
					x           = !isNaN(str.substring(1, 2)) ? 2 : 1;
					y           = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
					yearMatrix  = [x + y + 2, x + y + 6];
					monthMatrix = [0, x];
					dayMatrix   = [x + 1, y + x + 1];
					break;

				case 1053:
					yearMatrix  = [0, 4];
					monthMatrix = [5, 7];
					dayMatrix   = [8, 10];
					break;

				case 1035:
					x           = !isNaN(str.substring(1, 2)) ? 2 : 1;
					y           = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
					yearMatrix  = [x + y + 2, x + y + 6];
					monthMatrix = [x + 1, y + x + 1];
					dayMatrix   = [0, x];
					break;

				case 1061:
					x           = !isNaN(str.substring(1, 2)) ? 2 : 1;
					y           = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
					yearMatrix  = [x + y + 2, x + y + 6];
					monthMatrix = [x + 1, y + x + 1];
					dayMatrix   = [0, x];
					break;

				case 1063:
					yearMatrix  = [0, 4];
					monthMatrix = [5, 7];
					dayMatrix   = [8, 10];
					break;

	            case 1031:
	                x = !isNaN(str.substring(1, 2)) ? 2 : 1;
	                y = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
	                yearMatrix = [x + y + 2, x + y + 6];
	                monthMatrix = [x + 1, y + x + 1];
	                dayMatrix = [0, x];
	                break;

	            case 2052:
					x           = !isNaN(str.substring(6, 7)) ? 2 : 1;
					y           = str.length == 8 + x ? 2 : 1;
					yearMatrix  = [0, 4];
					monthMatrix = [5, 5 + x];
					dayMatrix   = [5 + x + 1, str.length];
					break;

				case 2057:
					x           = !isNaN(str.substring(1, 2)) ? 2 : 1;
					y           = !isNaN(str.substring(2 + x, 3 + x)) ? 2 : 1;
					yearMatrix  = [x + y + 2, x + y + 6];
					monthMatrix = [x + 1, y + x + 1];
					dayMatrix   = [0, x];
					break;

				default:
					yearMatrix  = [6, 10];
					monthMatrix = [3, 5];
					dayMatrix   = [0, 2];
			}

			yyyy = parseFloat(str.substring(yearMatrix[0], yearMatrix[1]));
			mm   = parseFloat(str.substring(monthMatrix[0], monthMatrix[1]));
			dd   = parseFloat(str.substring(dayMatrix[0], dayMatrix[1]));

			return new Date(yyyy, mm - 1, dd);
		},
		toDateString: function (date, props) {
            var del  = props && 'delimiter' in props ? props.delimiter : _.page.getDateDelimiter(),
				d    = date.getDate(),
				m    = date.getMonth() + 1,
				y    = date.getFullYear(),
                lang = props && 'lcid' in props ? props.lcid : _.page.lcid,
                id   = props && 'date' in props ? props.date : true,
                im   = props && 'month' in props ? props.month : true,
                iy   = props && 'year' in props ? props.year : true;

            var output = [];
            switch (lang) {
                case 1033:
                    if(im) { output.push(m); }
                    if(id) { output.push(d); }
                    if(iy) { output.push(y); }
                    break;
                case 1053:
                    d = d < 10 ? '0' + d : d;
					m = m < 10 ? '0' + m : m;
                    if(iy) { output.push(y); }
                    if(im) { output.push(m); }
                    if(id) { output.push(d); }
                    break;
                case 1035:
                    if(id) { output.push(d); }
                    if(im) { output.push(m); }
                    if(iy) { output.push(y); }
                    break;
                case 1061:
                    m = m < 10 ? '0' + m : m;
                    if(id) { output.push(d); }
                    if(im) { output.push(m); }
                    if(iy) { output.push(y); }
                    break;
                case 1063:
                    d = d < 10 ? '0' + d : d;
					m = m < 10 ? '0' + m : m;
                    if(iy) { output.push(y); }
                    if(im) { output.push(m); }
                    if(id) { output.push(d); }
                    break;
                case 1031:
                    d = d < 10 ? '0' + d : d;
                    m = m < 10 ? '0' + m : m;
                    if (id) { output.push(d); }
                    if (im) { output.push(m); }
                    if (iy) { output.push(y); }
                    break;
                case 2052:
                    if(iy) { output.push(y); }
                    if(im) { output.push(m); }
                    if(id) { output.push(d); }
                    break;
                case 2057:
                    if(id) { output.push(d); }
                    if(im) { output.push(m); }
                    if(iy) { output.push(y); }
                    break;
                case 3081:
                    m = m < 10 ? '0' + m : m;
                    if (id) { output.push(d); }
                    if (im) { output.push(m); }
                    if (iy) { output.push(y); }
                    break;
                default:
                    d = d < 10 ? '0' + d : d;
					m = m < 10 ? '0' + m : m;
                    if(id) { output.push(d); }
                    if(im) { output.push(m); }
                    if(iy) { output.push(y); }
                    break;
            }
            return output.join(''+del);
		},

		toTimeString: function(date, props) {
			var del = props && 'delimiter' in props ? props.delimiter : ':',
				h   = _.cc.toDblDigit(date.getHours()),
				m   = _.cc.toDblDigit(date.getMinutes()),
				s   = _.cc.toDblDigit(date.getSeconds()),
                ih  = props && 'hours' in props ? props.hours : true,
                im  = props && 'minutes' in props ? props.minutes : true,
                is  = props && 'seconds' in props ? props.seconds : false;

            var output  = [];
            if(ih) { output.push(h); }
            if(im) { output.push(m); }
            if(is) { output.push(s); }
            return output.join(''+del)
		},

		toDateTimeString: function(date, props) {
			var _datepart = this.toDateString(date, props);
			var _timepart = this.toTimeString(date, props);
			return _datepart + ' ' + _timepart;
		},

		toDateTime: function(str) {
			var dateA = str.split(' '),
				timeA = dateA[1].split(':'),
				date  = this.toDate(dateA[0]);

			timeA[0] && date.setHours(timeA[0]);
			timeA[1] && date.setMinutes(timeA[1]);
			timeA[2] && date.setSeconds(timeA[2]);
			timeA[3] && date.setMilliseconds(timeA[3]);

			return date;
		},

		dateTimeToText: function(date) {
            if(date === undefined || date === null)
                return '';

			var str = date.getFullYear(),
				tmp = (date.getMonth() + 1).toString();

			tmp.length == 1 && (tmp = '0' + tmp);
			str += tmp;

			tmp = date.getDate().toString();
			tmp.length == 1 && (tmp = '0' + tmp);
			str += tmp + 'T';

			tmp = date.getHours().toString();
			tmp.length == 1 && (tmp = '0' + tmp);
			str += tmp;

			tmp = date.getMinutes().toString();
			tmp.length == 1 && (tmp = '0' + tmp);
			str += tmp;

			tmp = date.getSeconds().toString();
			tmp.length == 1 && (tmp = '0' + tmp);
			str += tmp;

			return str;
		},

		dateToText: function(date) {
			var str = date.getFullYear(),
				tmp = (date.getMonth() + 1).toString();

			tmp.length == 1 && (tmp = '0' + tmp);
			str += tmp;

			tmp = date.getDate().toString();
			tmp.length == 1 && (tmp = '0' + tmp);
			str += tmp + 'T000000';

			return str;
		},

		textToDateTime: function(str) {
            if(str === undefined || str === null || str === '')
                return null;

			var y = parseInt(str.substr(0, 4)),
				m = str.substr(4, 1) == '0' ? parseInt(str.substr(5, 1)) - 1 : parseInt(str.substr(4, 2)) - 1,
				d = str.substr(6, 1) == '0' ? parseInt(str.substr(7, 1)) : parseInt(str.substr(6, 2)),
				h = str.substr(9, 1) == '0' ? parseInt(str.substr(10, 1)) : parseInt(str.substr(9, 2)),
				n = str.substr(11, 1) == '0' ? parseInt(str.substr(12, 1)) : parseInt(str.substr(11, 2)),
				s = str.substr(13, 1) == '0' ? parseInt(str.substr(14, 1)) : parseInt(str.substr(13, 2));
			return new Date(y, m, d, h, n, s);
		},

		toNumeric: function(value, failValue) {
			return (isNaN(value) || value == '') ? (isNaN(failValue) || failValue == '') ? Number.MIN_VALUE : parseInt(failValue) : parseInt(value);
		},

		toDecimal: function(value) {
			return parseFloat(value.replace(',', '.'));
		},

		toHcp: function(value) {
			var hcp = this.toDecimal(value);
			value.indexOf('+') == 0 && (hcp = -hcp);
			return hcp;
		},

		hcpToText: function(hcp, lcid) {
            if(hcp === undefined || hcp === null || hcp === "")
                return "";
		    var sep = _.cc.getNumberSeparator(),
				tHcp = (typeof hcp === 'number') ? hcp : this.toHcp(hcp),
				dec  = (tHcp >= 37) ? 0 : 1;

			if (tHcp < 0)
				return '+' + Math.abs(tHcp).toFixed(dec).toString().replace(/\./g, sep);

			if (tHcp > 36 && /(,|\.)[1-9]/ig.test(tHcp.toFixed(2).toString()))
				dec = 1;

			return tHcp.toFixed(dec).toString().replace(/\./g, sep);
		},

		getNumberSeparator: function (lcid) {
		    var lang = lcid || _.page.lcid;
		    return (lang == 1030 || lang == 1044 || lang == 1053 || lang == 1035 || lang == 1061 || lang == 1063 || lang == 1031) ? ',' : '.';
		},

		formatDecimal: function (value, decimals, forceDecimals) {
		    if (value === '')
		        return '';
		    if (decimals == undefined || decimals == null)
		        decimals = 1;
		    var parsed = this.toDecimal(value.toString());
		    return forceDecimals !== true && parsed % 1 === 0 ? parsed : parsed.toFixed(decimals).replace('.', this.getNumberSeparator());
		},

		timeUnitToMinutes: function(minutes, unit) {
			switch (unit) {
				case 0: return minutes;        // minutes
				case 1: return minutes * 60;   // hours
				case 2: return minutes * 1440; // days
			}
		},

		toDblDigit: function(value) {
			return value < 10 ? '0' + value : value;
		},

        calculateAge: function (birthDate, ageDate) {
            // ageDate must be higher than birthDate
            if (ageDate < birthDate) {
                return null;
            }

            var years = 0;
            var days = (ageDate - birthDate) / (1000 * 3600 * 24);

            var currentYear = ageDate.getFullYear();
            while (days >= 365) {
                var tmp = new Date(currentYear, 1, 28); // 28th of February that year
                tmp.setDate(tmp.getDate() + 1);
                var isLeapYear = tmp.getMonth() == 1;

                var daysThisYear = isLeapYear ? 366 : 365;
                if (days >= daysThisYear) {
                    years++;
                }

                days -= daysThisYear;
                currentYear--;
            }


            return years;
        }

	};

})(jQuery, window);