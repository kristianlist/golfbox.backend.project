/**
 * @preserve golfbox.lib-page.js
 */
(function($, window, undefined) {
	if (window.golfbox === undefined)
		window.golfbox = window.gb = {};

	var document  = window.document,
		navigator = window.navigator,
		location  = window.location,
		_         = window.golfbox;
	_.page = {
    
		lcid           : 1030,
		countryISOCode : 'DK',
		debug          : false,
		log            : { holderId : 'logHolder' },
		dpRegional     : undefined,
        scrollbarWidth : 20,

		getDateDelimiter: function() {
			switch (this.lcid) {
				case 1031: return '.';
				case 1030: return '-';
				case 1044: return '.';
				case 1053: return '-';
				case 1035: return '.';
				case 1061: return '.';
				case 1063: return '.';
				case 2052: return '-';
				default:   return '/';
			}
		},

		getCountryISOCode: function() {
			switch (this.lcid) {
				case 1031: return 'DE';
				case 1030: return 'DK';
				case 1044: return 'NO';
				case 1053: return 'SE';
				case 1035: return 'FI';
				case 1061: return 'EE';
				case 1063: return 'LT';
				case 2052: return 'CN';
				case 1033: return 'US';
				case 2057: return 'UK';
				case 3081: return 'AU';
				default:   return 'DK';
			}
		}
	};

})(jQuery, window);