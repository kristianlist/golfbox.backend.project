﻿/**
 * @preserve golfbox.lib-utility.js
 */
(function ($, window, undefined) {
    if (window.golfbox === undefined)
        window.golfbox = window.gb = {};

    var document = window.document,
		navigator = window.navigator,
		location = window.location,
		_ = window.golfbox;

    _.util = _.utility = {
        is: {
            number: function (value) {
                return !isNaN(value - 0) && value !== null && value !== '' && value !== false;
            },
            nothing: function (value) {
                return value === null || value === undefined || value === '';
            }
        },
        object: {
            get: function (path, obj) {
                if (gb.util.is.nothing(path) || gb.util.is.nothing(obj))
                    return obj;

                var paths = path.split('.');
                for (var p = 0; p < paths.length; p++) {
                    if (gb.util.is.nothing(obj))
                        break;

                    obj = obj[paths[p]];
                }
                return obj;

            }
        },
        array: {
            contains: function (value, path, array, allowTypeConversion) {
                return _.util.array.find(value, path, array, allowTypeConversion) !== undefined;
            },
            find: function (value, path, array, allowTypeConversion) {
                return array[_.util.array.index(value, path, array, allowTypeConversion)];
            },
            index: function (value, path, array, allowTypeConversion) {
                var tmp = undefined;
                for (var index in array) {
                    tmp = _.util.object.get(path, array[index]);
                    if (tmp === value || (allowTypeConversion && tmp == value))
                        return index;
                }
            },
            add: function(from,to) {
                for (var i = 0; i < from.length; i++) {
                    to.push(from[i]);
                }
            },
            distinct: function (array) {
                var found = {};
                return array.filter(function (item) {
                    if (typeof item === 'object') {
                        var hash = getHashFromObject(item);
                        return found.hasOwnProperty(hash) ? false : (found[hash] = true);
                    } else {
                        return found.hasOwnProperty(item) ? false : (found[item] = true);
                    }
                });
            },
            search: function (value, paths, array, allowTypeConversion, caseSensitive, allowPartialMatches) {
                var matches = [],
                    tmp = undefined;
                for (var index in array) {
                    for (var path in paths) {
                        tmp = _.util.object.get(paths[path], array[index]);
                        if (!gb.util.is.nothing(tmp)) {
                            if (allowPartialMatches) {
                                if (!caseSensitive) {
                                    if (tmp.toString().toLowerCase().indexOf(value.toLowerCase()) > -1) {
                                        matches.push(array[index]);
                                    }
                                } else {
                                    if (tmp.toString().indexOf(value) > -1) {
                                        matches.push(array[index]);
                                    }
                                }
                            }
                            else {
                                if (!caseSensitive) {
                                    if (tmp.toString().toLowerCase() === value.toString().toLowerCase()) {
                                        matches.push(array[index]);
                                    }
                                } else {
                                    if (tmp === value || (allowTypeConversion && tmp == value)) {
                                        matches.push(array[index]);
                                    }
                                }
                            }
                        }
                    }
                }

                return gb.util.array.distinct(matches);
            }
        }
    };

    var getHashFromObject = function (o) {
        var hash = '';
        for (var key in o) {
            if (typeof o[key] === 'object') {
                hash += getHashFromObject(o[key]);
            } else {
                hash += o[key];
            }
        }
        return hash;
    };

})(jQuery, window);