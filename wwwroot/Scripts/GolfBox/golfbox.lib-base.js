/**
 * @license GolfBox JavaScript Library
 * golfbox.lib-base.js
 */
(function($, window, undefined) {
	if (window.golfbox === undefined)
		window.golfbox = window.gb = {};

	var document  = window.document,
		navigator = window.navigator,
		location  = window.location,
		_         = window.golfbox;

	_.log = function(msg, force) {
		var init = function(m) {
			if (!(_.ui.log && _.ui.log(m))) {
				window.console && window.console.log && window.console.log(m);
			}
		};

		if (force) {
			init(msg);
		} else {
			_.page.debug && init(msg);
		}
	};

	_.redirect = function(url) {
		if (location.assign)
			location.assign(url);
		else
			location = url;
	};

	_.openWindow = function(link, width, height, name, scroll) {
		var left = (window.screen.width - width) / 2,
			top  = (window.screen.height - height) / 2;
		this.openerWin = window.open(link, name, 'height=' + height + ',width=' + width + ',top=' + top + ',left=' + left + ',scrollbars=' + (typeof scroll == 'number' ? scroll : '0') + ',resizable=0,status=0,toolbar=0,location=0,menubar=0,directories=0');
		if (parseInt(navigator.appVersion) >= 4 && this.openerWin && this.openerWin.focus)
			this.openerWin.focus();
	};

	_.utf8_encode = function(text) {
		var str = text.replace(/\r\n/g, '\n'),
			utf = [];

		for (var n = 0; n < str.length; n++) {
			var c = str.charCodeAt(n);
			if (c < 128) {
				utf.push(String.fromCharCode(c));
			} else if ((c > 127) && (c < 2048)) {
				utf.push(String.fromCharCode((c >> 6) | 192));
				utf.push(String.fromCharCode((c & 63) | 128));
			} else {
				utf.push(String.fromCharCode((c >> 12) | 224));
				utf.push(String.fromCharCode(((c >> 6) & 63) | 128));
				utf.push(String.fromCharCode((c & 63) | 128));
			}
		}

		return utf.join('');
	};

	_.utf8_decode = function(utf) {
		var str = [],
			i   = 0,
			c1  = 0,
			c2  = 0,
			c3  = 0;

		while (i < utf.length) {
			c1 = utf.charCodeAt(i);
			if (c1 < 128) {
				str.push(String.fromCharCode(c1));
				i += 1;
			} else if ((c1 > 191) && (c1 < 224)) {
				c2 = utf.charCodeAt(i + 1);
				str.push(String.fromCharCode(((c1 & 31) << 6) | (c2 & 63)));
				i += 2;
			} else {
				c2 = utf.charCodeAt(i + 1);
				c3 = utf.charCodeAt(i + 2);
				str.push(String.fromCharCode(((c1 & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63)));
				i += 3;
			}
		}

		return str.join('');
	};

	_.encode = function(txt) {
		return escape(this.utf8_encode(txt));
	};

	_.decode = function(txt) {
		return this.utf8_decode(unescape(txt));
	};

	_.nextFieldFocus = function(field, nextFieldId) {
		if (field.value.length >= field.maxLength)
			document.getElementById(nextFieldId).focus();
	};

	_.getViewportWidthHeight = function() {
		return { width: $(window).width(), height: $(window).height() };
	};

	_.getDocumentWidthHeight = function() {
		return { width: $(document).width(), height: $(document).height() };
	};

	_.getRandomNumber = function(minimum, maximum) {
		return Math.floor(Math.random() * (maximum - minimum + 1)) + minimum;
	};

	var _uniqueResults = {};
	_.getUniqueNumber = function(length) {
        if(typeof length !== 'number') {
            length = 13;
        }

		var result = [];
		
        while (result.length < length) {
			var r = this.getRandomNumber(0, 9).toString();

			if (result.length == 0 && r == '0')
				continue;

			result.push(r);
		}

        result = result.join('');

        if (_uniqueResults[result]) {
            return _.getUniqueNumber(length);
        } else {
            _uniqueResults[result] = 1;
        }

		return (+result);
	};

	_.fLabel = function(width, text, id, styles, showTitle, title) {
		//javascript version of WebSite.Common.Utility.FLabel
		var sTitle = (typeof showTitle == 'boolean') ? showTitle : true;

		if (sTitle && (title || text))
			return '<div ' + ((id) ? 'id="' + id + '" ' : '') + 'class="fixLength" style="width: ' + width + 'px' + ((styles) ? '; ' + styles : '') + '" title="' + ((title) ? title : text).toString().replace(/\"/ig, '&quot;') + '">' + text + '</div>';
		return '<div ' + ((id) ? 'id="' + id + '" ' : '') + 'class="fixLength" style="width: ' + width + 'px' + ((styles) ? '; ' + styles : '') + '">' + text + '</div>';
	};

	_.changeInputTime = function(obj, showError) {
		var value = $.trim(obj.value);

		if (value == '') return;

		var parts        = [],
			errorText    = 'The time: #DateValue#, could not be re-formatted',
			patternFound = false,
			patterns     = [
				{ k: 'd',       v: /^\d{1}$/ },
				{ k: 'dd',      v: /^\d{2}$/ },
				{ k: 'ddd',     v: /^\d{3}$/ },
				{ k: 'dddd',    v: /^\d{4}$/ },
				{ k: 'generic', v: /^\d{1,2}(-|\.|\/|,|:)\d{1,2}$/ }
			];

		var getParts = function(format) {
			switch (format) {
				case 'd':
					return [
						parseInt(value, 10),
						0
					];

				case 'dd':
					return [
						parseInt(value, 10),
						0
					];

				case 'ddd':
					return [
						parseInt(value.substring(0, 1), 10),
						parseInt(value.substring(1, 3), 10)
					];

				case 'dddd':
					return [
						parseInt(value.substring(0, 2), 10),
						parseInt(value.substring(2, 4), 10)
					];

				case 'generic':
					var p = [0, 0],
						m = value.match(/^(\d+)(-|\.|\/|,|:)(\d+)$/);

					if (m == null)
						return p;

					p[0] = parseInt(m[1], 10);

					if (m[3].length == 1)
						m[3] = m[3] + '0';

					p[1] = (m[3] < 10) ? parseInt('0' + m[3], 10) : parseInt(m[3], 10);

					return p;
			}
		};

		var isValidHour = function(val) {
			var v = (+val);
			return (v <= 23);
		}

		var isValidMinute = function(val) {
			var v = (+val);
			return (v <= 59);
		}

		var padZeros = function(val) {
			var v = (+val);
			if (v < 10)
				return '0' + v;
			return v;
		};

		for (var i = 0; i < patterns.length; i++) {
			if (!patternFound && patterns[i].v.test(value)) {
				patternFound = true;
				parts = getParts(patterns[i].k);
			}
		}

		if (!patternFound) {
			if (showError || showError === undefined)
				alert(errorText.replace(/#DateValue#/ig, value));
			return false;
		}

		if (isValidHour(parts[0]) && isValidMinute(parts[1])) {
			var result = 'hh:mm'.replace(/hh/, padZeros(parts[0]));
			result = result.replace(/mm/, padZeros(parts[1]));

			obj.value = result;
			return true;
		}

		if (showError || showError === undefined)
			alert(errorText.replace(/#DateValue#/ig, value));
		return false;
	};

	_.changeInputDate = function(obj, showError) {
		var value = $.trim(obj.value);

		if (value == '') return;

		var parts        = [],
			errorText    = 'The date: #DateValue#, could not be re-formatted',
			patternFound = false,
			patterns     = [
				{ k: 'mmdd',       v: /^\d{4}$/ },
				{ k: 'mm#dd',      v: /^\d{2}(-|\.|\/)\d{2}$/ },
				{ k: 'yymmdd',     v: /^\d{6}$/ },
				{ k: 'yyyymmdd',   v: /^\d{8}$/ },
				{ k: 'yy#mm#dd',   v: /^\d{2}(-|\.|\/)\d{2}(-|\.|\/)\d{2}$/ },
				{ k: 'yyyy#mm#dd', v: (_.page.lcid == 1053 || _.page.lcid == 2057) ? /^\d{4}(-|\.|\/)\d{2}(-|\.|\/)\d{2}$/ : /^\d{2}(-|\.|\/)\d{2}(-|\.|\/)\d{4}$/ },
				{ k: 'mm#dd#yyyy', v: /^\d{2}(-|\.|\/)\d{2}(-|\.|\/)\d{4}$/ },
				{ k: 'm#d#yyyy',   v: /^\d{1}(-|\.|\/)\d{1}(-|\.|\/)\d{4}$/ },
				{ k: 'm#dd#yyyy',  v: /^\d{1}(-|\.|\/)\d{2}(-|\.|\/)\d{4}$/ },
				{ k: 'mm#d#yyyy',  v: /^\d{2}(-|\.|\/)\d{1}(-|\.|\/)\d{4}$/ },
				{ k: 'm#d#yy',     v: /^\d{1}(-|\.|\/)\d{1}(-|\.|\/)\d{2}$/ },
				{ k: 'm#dd#yy',    v: /^\d{1}(-|\.|\/)\d{2}(-|\.|\/)\d{2}$/ },
				{ k: 'mm#d#yy',    v: /^\d{2}(-|\.|\/)\d{1}(-|\.|\/)\d{2}$/ },
				{ k: 'm#d',        v: /^\d{1}(-|\.|\/)\d{1}$/ },
				{ k: 'mm#d',       v: /^\d{2}(-|\.|\/)\d{1}$/ },
				{ k: 'm#dd',       v: /^\d{1}(-|\.|\/)\d{2}$/ }
			];

		var getParts = function(format) {
			switch (format) {
				case 'mmdd':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								(new Date()).getFullYear(),
								value.substring(0, 2),
								value.substring(2, 4)
							];

						default:
							return [
								(new Date()).getFullYear(),
								value.substring(2, 4),
								value.substring(0, 2)
							];
					}

				case 'mm#dd':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								(new Date()).getFullYear(),
								value.substring(0, 2),
								value.substring(3, 5)
							];

						default:
							return [
								(new Date()).getFullYear(),
								value.substring(3, 5),
								value.substring(0, 2)
							];
					}

				case 'yymmdd':
					switch (_.page.lcid) {
						case 1053:
							return [
								value.substring(0, 2),
								value.substring(2, 4),
								value.substring(4, 6)
							];

						case 2057:
							return [
								value.substring(4,6),
								value.substring(0,2),
								value.substring(2,4)
							];

						default:
							return [
								value.substring(4, 6),
								value.substring(2, 4),
								value.substring(0, 2)
							];
					}

				case 'yyyymmdd':
					switch (_.page.lcid) {
						case 1053:
							return [
								value.substring(0, 4),
								value.substring(4, 6),
								value.substring(6, 8)
							];

						case 2057:
							return [
								value.substring(4, 8),
								value.substring(0, 2),
								value.substring(2, 4)
							];

						default:
							return [
								value.substring(4, 8),
								value.substring(2, 4),
								value.substring(0, 2)
							];
					}

				case 'yy#mm#dd':
					switch (_.page.lcid) {
						case 1053:
							return [
								value.substring(0, 2),
								value.substring(3, 5),
								value.substring(6, 8)
							];

						case 2057:
							return [
								value.substring(6, 8),
								value.substring(0, 2),
								value.substring(3, 5)
							];

						default:
							return [
								value.substring(6, 8),
								value.substring(3, 5),
								value.substring(0, 2)
							];
					}

				case 'yyyy#mm#dd':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								value.substring(0, 4),
								value.substring(5, 7),
								value.substring(8, 10)
							];

						default:
							return [
								value.substring(6, 10),
								value.substring(3, 5),
								value.substring(0, 2)
							];
					}

				case 'mm#dd#yyyy':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								value.substring(6, 10),
								value.substring(0, 2),
								value.substring(3, 5)
							];

						default:
							return [
								value.substring(6, 10),
								value.substring(3, 5),
								value.substring(0, 2)
							];
					}

				case 'm#d#yyyy':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								value.substring(4, 8),
								'0' + value.substring(0, 1),
								'0' + value.substring(2, 3)
							];

						default:
							return [
								value.substring(4, 8),
								'0' + value.substring(2, 3),
								'0' + value.substring(0, 1)
							];
					}

				case 'm#dd#yyyy':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								value.substring(5, 9),
								'0' + value.substring(0, 1),
								value.substring(2, 4)
							];

						default:
							return [
								value.substring(5, 9),
								value.substring(2, 4),
								'0' + value.substring(0, 1)
							];
					}

				case 'mm#d#yyyy':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								value.substring(5, 9),
								value.substring(0, 2),
								'0' + value.substring(3, 4)
							];

						default:
							return [
								value.substring(5, 9),
								'0' + value.substring(3, 4),
								value.substring(0, 2)
							];
					}

				case 'm#d#yy':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								value.substring(4, 6),
								'0' + value.substring(0, 1),
								'0' + value.substring(2, 3)
							];

						default:
							return [
								value.substring(4, 6),
								'0' + value.substring(2, 3),
								'0' + value.substring(0, 1)
							];
					}

				case 'm#dd#yy':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								value.substring(5, 7),
								'0' + value.substring(0, 1),
								value.substring(2, 4)
							];

						default:
							return [
								value.substring(5, 7),
								value.substring(2, 4),
								'0' + value.substring(0, 1)
							];
					}

				case 'mm#d#yyyy':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								value.substring(5, 7),
								value.substring(0, 2),
								'0' + value.substring(3, 4)
							];

						default:
							return [
								value.substring(5, 7),
								'0' + value.substring(3, 4),
								value.substring(0, 2)
							];
					}

				case 'm#d':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								(new Date()).getFullYear(),
								'0' + value.substring(0, 1),
								'0' + value.substring(2, 3)
							];

						default:
							return [
								(new Date()).getFullYear(),
								'0' + value.substring(2, 3),
								'0' + value.substring(0, 1)
							];
					}

				case 'mm#d':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								(new Date()).getFullYear(),
								value.substring(0, 2),
								'0' + value.substring(3, 4)
							];

						default:
							return [
								(new Date()).getFullYear(),
								'0' + value.substring(3, 4),
								value.substring(0, 2)
							];
					}

				case 'm#dd':
					switch (_.page.lcid) {
						case 1053:
						case 2057:
							return [
								(new Date()).getFullYear(),
								'0' + value.substring(0, 1),
								value.substring(2, 4)
							];

						default:
							return [
								(new Date()).getFullYear(),
								value.substring(2, 4),
								'0' + value.substring(0, 1)
							];
					}
			}
		};

		var isValidMonth = function(val) {
			var v = (+val);
			return (v >= 1 && v <= 12);
		};

		var isValidDay = function(val) {
			var v = (+val);
			return (v >= 1 && v <= 31);
		};

		var padZeros = function(val) {
			var v = (+val);
			if (v < 10)
				return '0' + v;
			return v;
		};

		var getFullYear = function(val) {
			var v = (+val);
			if (v > 1000)
				return v;
			if (v < 20)
				return '20' + padZeros(val);
			return '19' + padZeros(val);
		};

		for (var i = 0; i < patterns.length; i++) {
			if (!patternFound && patterns[i].v.test(value)) {
				patternFound = true;
				parts = getParts(patterns[i].k);
			}
		}

		if (!patternFound) {
			if (showError || showError === undefined)
				alert(errorText.replace(/#DateValue#/ig, value));
			return false;
		}

		if (isValidMonth(parts[1]) && isValidDay(parts[2])) {
			var result = (_.page.lcid == 1053) ? 'yyyy#mm#dd' : (_.page.lcid == 2057) ? 'mm#dd#yyyy' : 'dd#mm#yyyy';
			result = result.replace(/yyyy/, getFullYear(parts[0]));
			result = result.replace(/mm/, parts[1]);
			result = result.replace(/dd/, parts[2]);
			result = result.replace(/#/g, _.page.getDateDelimiter());

			obj.value = result;
			return true;
		}

		if (showError || showError === undefined)
			alert(errorText.replace(/#DateValue#/ig, value));
		return false;
	};

	_.changeInputHcp = function(obj, showError) {
		var value = $.trim(obj.value);

		if (value == '') return;

		var errorText = 'The hcp: #HcpValue#, could not be re-formatted';

		var isValidHcp = function(val) {
			var tmpHcp    = _.cc.toHcp(val);
			var isHcp     = /^(-|\+)?\d{1,3}((,|\.)\d)?$/.test(val);
			var isInRange = (tmpHcp >= -99.9 && tmpHcp <= 999.9);
			return (isInRange && isHcp);
		};

		if (isValidHcp(value)) {
			var sep    = _.cc.hcpToText(20).replace(/[0-9]/ig, '');
			var tHcp   = _.cc.toHcp(value);
			var dec    = (tHcp >= 37) ? 0 : 1;
			var result = '';

			if (tHcp < 0) {
				result = '+' + Math.abs(tHcp).toFixed(dec).toString().replace(/\./g, sep);
			} else {
				if (tHcp > 36 && /(,|\.)[1-9]/ig.test(value))
					dec = 1;
				result = tHcp.toFixed(dec).toString().replace(/\./g, sep);
			}

			obj.value = result;
			return true;
		}

		if (showError || showError === undefined)
			alert(errorText.replace(/#HcpValue#/ig, value));
		return false;
	};

    _.showNotAllowedMessage = function () {
        var error = golfbox.ui.systemmodal({ title: GBTranslations.NotAvailable, html: '<div class="modalcontent">' + GBTranslations.FeatureNotAvailableForTrialLicenses + '</div>' });
        error.addButton('notAllowedMessageOK', GBTranslations.OK, 'golfbox.ui.systemmodalOpener.close();');
    };

})(jQuery, window);