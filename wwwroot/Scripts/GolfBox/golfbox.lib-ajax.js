/**
 * @preserve golfbox.lib-ajax.js
 */
(function($, window, undefined) {
	if (window.golfbox === undefined)
		window.golfbox = window.gb = {};

	var document  = window.document,
		navigator = window.navigator,
		location  = window.location,
		_         = window.golfbox;

	_.ajax = {
		enabled: true,

		request: function(type, dataType, url, data, callback,async,error) {
			if (!this.enabled)
				return;

			var options = {
					type     : type,
                    async    : (typeof async == 'boolean' && !async) ? false:true,
					dataType : dataType,
					cache    : false,
					url      : url,
					data     : data,
					before   : undefined,
					callback : callback,
					error    : error,
					complete : undefined,
					timeout  : 0,
					endAjax  : false
				},
				req = function() {
					return $.ajax({
						global     : false,
						type       : options.type,
                        async      : options.async,
						dataType   : options.dataType,
						cache      : options.cache,
						url        : options.url,
						data       : options.data,
						timeout    : options.timeout,
						beforeSend : function(xhr, settings) {
							if ($.isFunction(options.before))
								options.before(xhr, settings);
						}
					})
					.fail(function(xhr, textStatus, errorThrown) {
                        if ($.isFunction(options.error))
                            options.error(xhr, textStatus, errorThrown);
						if(xhr.status === 401 || xhr.status === 403) {
                            window.golfbox.ajax.getJSON('/ClientHandlers/Security/SecurityHandler.ashx?methodName=Alive',{},function(alive){
                                if(alive == 0)
                                    location.reload(true);
                            });
                        }
					})
					.done(function(data, textStatus, xhr) {
                        if ($.isFunction(options.callback))
							options.callback(data, textStatus, xhr);
					})
					.complete(function(xhr, textStatus) {
						if ($.isFunction(options.complete))
							options.complete(xhr, textStatus);
						_.log(textStatus + ': ' + options.url);
					});
				};

			if ($.isFunction(data)) {
				options.callback = data;
				options.data     = undefined;
			} else if (typeof callback === 'object') {
				$.extend(true, options, callback);

				if (options.endAjax) {
					this.enabled = false;
				}
			}

			return req();
		},

		get: function (url, data, callback, async,error) {
		    return this.request('get', undefined, url, data, callback, (async !== null && async !== undefined && !async) ? false:true, error);
		},

		getJSON: function(url, data, callback, async,error) {
		    return this.request('get', 'json', url, data, callback, (async !== null && async !== undefined && !async) ? false : true, error);
		},

		post: function (url, data, callback, async,error) {
		    return this.request('post', undefined, url, data, callback, (async !== null && async !== undefined && !async) ? false : true, error);
		}
	};

})(jQuery, window);