﻿$('#view-menu').on('click', '[data-view]', function () { 
    var $this   = $(this);
    var view    = $this.attr('data-view');
    console.log(view);
    $('#view-rules, #view-users, #view-issues').hide();

    switch (view.toLowerCase()) {
        case "rules":
            golfbox.ajax.post('/Handler/getrulescontrol', indata, function (outdata) {
                //console.log(outdata);
                list_rules_current.items = outdata[0];
                list_rules_current.render();
                list_rules_candidates.items = outdata[1];
                list_rules_candidates.render();
                list_rules_backlog.items = outdata[2];
                list_rules_backlog.render();
                $('#view-rules').show();
            });
            break;
        case "users":
            golfbox.ajax.post('/Handler/getusers', indata, function (outdata) {
                //console.log(outdata);
                list_users.items = outdata;
                list_users.render();
                $('#view-users').show();
            });
            break;
        case "issues":
            golfbox.ajax.post('/Handler/getcurrentissues', indata, function (outdata) {
                //console.log(outdata);
                list_issues_red.items = outdata[0];
                list_issues_red.render();
                list_issues_yellow.items = outdata[1];
                list_issues_yellow.render();
                $('#view-issues').show();
            });
            break;
        case "update":
            golfbox.ajax.post('/Handler/updatefromredmine', indata, function (outdata) {
                alert(outdata);
            });
            break;
        case "gantt":
            $('#view-gantt').show();
            gantt();
            break;
    }

    $this.addClass('active').siblings('.active').removeClass('active');
});

$(window).on('load', function () {
    console.log('loaded');
    $('#view-menu div[data-view="gantt"]').trigger('click');
});

var list_rules_current = golfbox.ui.list.create('#view-rules-current', { 'title' : 'Igangværende' });
list_rules_current.columns.push({ 'property': 'Link', 'width': '70px', 'title': 'ID', 'textAlign': 'right' });
list_rules_current.columns.push({ 'property': 'ProjectName', 'width': '150px', 'title': 'Project' });
list_rules_current.columns.push({ 'property': 'Subject', 'width': '390px', 'title': 'subject' });
list_rules_current.columns.push({ 'property': 'AssigneeInitals', 'width': '50px', 'title': 'Init' });
list_rules_current.columns.push({ 'property': 'FirstErrorText', 'width': '300px', 'title': 'error' });

var list_rules_candidates = golfbox.ui.list.create('#view-rules-candidates', { 'title': 'Kandidater' });
list_rules_candidates.columns.push({ 'property': 'Link', 'width': '70px', 'title': 'ID', 'textAlign': 'right' });
list_rules_candidates.columns.push({ 'property': 'ProjectName', 'width': '150px', 'title': 'Project' });
list_rules_candidates.columns.push({ 'property': 'Subject', 'width': '390px', 'title': 'subject' });
list_rules_candidates.columns.push({ 'property': 'AssigneeInitals', 'width': '50px', 'title': 'Init' });
list_rules_candidates.columns.push({ 'property': 'FirstErrorText', 'width': '300px', 'title': 'error' });

var list_rules_backlog = golfbox.ui.list.create('#view-rules-backlog', { 'title': 'Backlog' });
list_rules_backlog.columns.push({ 'property': 'Link', 'width': '70px', 'title': 'ID', 'textAlign': 'right' });
list_rules_backlog.columns.push({ 'property': 'ProjectName', 'width': '150px', 'title': 'Project' });
list_rules_backlog.columns.push({ 'property': 'Subject', 'width': '390px', 'title': 'subject' });
list_rules_backlog.columns.push({ 'property': 'AssigneeInitals', 'width': '50px', 'title': 'Init' });
list_rules_backlog.columns.push({ 'property': 'FirstErrorText', 'width': '300px', 'title': 'error' });




var list_users = golfbox.ui.list.create('#view-users', { 'title' : 'User Status' });
list_users.columns.push({ 'property': 'Initials', 'width': '50px', 'title': 'Init' });
list_users.columns.push({ 'property': 'Fullname', 'width': '200px' });
list_users.columns.push({ 'property': 'IssuesCurrent', 'width': '100px', 'title': 'Current', 'textAlign': 'right' });
list_users.columns.push({ 'property': 'EstimatedHoursCurrent', 'width': '60px', 'title': 'Est', 'textAlign': 'right', 'format': 'decimal' });
list_users.columns.push({ 'property': 'IssuesCandidates', 'width': '100px', 'title': 'Candidates', 'textAlign': 'right' });
list_users.columns.push({ 'property': 'EstimatedHoursCandidates', 'width': '60px', 'title': 'Est', 'textAlign': 'right', 'format': 'decimal' });
list_users.columns.push({ 'property': 'LatestSpentTime', 'width': '150px', 'title': 'Last Reg', 'textAlign': 'right' });
list_users.columns.push({ 'property': 'HoursSpentThisWeek', 'width': '120px', 'title': 'Spent Week', 'textAlign': 'right', 'format': 'decimal' });
list_users.columns.push({ 'property': 'HoursSpentLastWeek', 'width': '120px', 'title': 'Spent Week-1', 'textAlign': 'right', 'format': 'decimal' });




var list_issues_red = golfbox.ui.list.create('#view-issues-red', { 'title' : 'Red' });
list_issues_red.columns.push({ 'property': 'Link', 'width': '70px', 'title': 'ID', 'textAlign': 'right' });
list_issues_red.columns.push({ 'property': 'ProjectName', 'width': '150px', 'title': 'Project' });
list_issues_red.columns.push({ 'property': 'Subject', 'width': '230px', 'title': 'subject' });
list_issues_red.columns.push({ 'property': 'AssigneeInitals', 'width': '50px', 'title': 'Init' });
list_issues_red.columns.push({ 'property': 'StatusName', 'width': '130px', 'title': 'Status' });
list_issues_red.columns.push({ 'property': 'EstimatedHours', 'width': '75px', 'title': 'Est', 'textAlign': 'right', 'format': 'decimal' });
list_issues_red.columns.push({ 'property': 'EstimatedRisk', 'width': '75px', 'title': 'Risk%', 'textAlign': 'right' });
list_issues_red.columns.push({ 'property': 'DoneRatio', 'width': '75px', 'title': 'Done%', 'textAlign': 'right' });
list_issues_red.columns.push({ 'property': 'HoursSpent', 'width': '75px', 'title': 'Spent', 'textAlign': 'right', 'format': 'decimal' });
list_issues_red.columns.push({ 'property': 'SavedEstimatedLimitRatio', 'width': '75px', 'title': '% Lost+', 'textAlign': 'right', 'format': 'decimal' });

var list_issues_yellow = golfbox.ui.list.create('#view-issues-yellow', { 'title' : 'Yellow' });
list_issues_yellow.columns.push({ 'property': 'Link', 'width': '70px', 'title': 'ID', 'textAlign': 'right' });
list_issues_yellow.columns.push({ 'property': 'ProjectName', 'width': '150px', 'title': 'Project' });
list_issues_yellow.columns.push({ 'property': 'Subject', 'width': '230px', 'title': 'subject' });
list_issues_yellow.columns.push({ 'property': 'AssigneeInitals', 'width': '50px', 'title': 'Init' });
list_issues_yellow.columns.push({ 'property': 'StatusName', 'width': '130px', 'title': 'Status' });
list_issues_yellow.columns.push({ 'property': 'EstimatedHours', 'width': '75px', 'title': 'Est', 'textAlign': 'right', 'format': 'decimal' });
list_issues_yellow.columns.push({ 'property': 'EstimatedRisk', 'width': '75px', 'title': 'Risk%', 'textAlign': 'right' });
list_issues_yellow.columns.push({ 'property': 'DoneRatio', 'width': '75px', 'title': 'Done%', 'textAlign': 'right' });
list_issues_yellow.columns.push({ 'property': 'HoursSpent', 'width': '75px', 'title': 'Spent', 'textAlign': 'right', 'format': 'decimal' });
list_issues_yellow.columns.push({ 'property': 'SavedEstimatedHours', 'width': '75px', 'title': 'Lost', 'textAlign': 'right', 'format': 'decimal' });

var list_issues_green = golfbox.ui.list.create('#view-issues-green', { 'title' : 'Green' });
list_issues_green.columns.push({ 'property': 'Link', 'width': '70px', 'title': 'ID', 'textAlign': 'right' });
list_issues_green.columns.push({ 'property': 'ProjectName', 'width': '150px', 'title': 'Project' });
list_issues_green.columns.push({ 'property': 'Subject', 'width': '230px', 'title': 'subject' });
list_issues_green.columns.push({ 'property': 'AssigneeInitals', 'width': '50px', 'title': 'Init' });
list_issues_green.columns.push({ 'property': 'StatusName', 'width': '130px', 'title': 'Status' });
list_issues_green.columns.push({ 'property': 'EstimatedHours', 'width': '75px', 'title': 'Est', 'textAlign': 'right', 'format': 'decimal' });
list_issues_green.columns.push({ 'property': 'EstimatedRisk', 'width': '75px', 'title': 'Risk%', 'textAlign': 'right' });
list_issues_green.columns.push({ 'property': 'DoneRatio', 'width': '75px', 'title': 'Done%', 'textAlign': 'right' });
list_issues_green.columns.push({ 'property': 'HoursSpent', 'width': '75px', 'title': 'Spent', 'textAlign': 'right', 'format': 'decimal' });
list_issues_green.columns.push({ 'property': 'SavedEstimatedHours', 'width': '75px', 'title': 'Saved', 'textAlign': 'right', 'format': 'decimal' });

var indata = {};