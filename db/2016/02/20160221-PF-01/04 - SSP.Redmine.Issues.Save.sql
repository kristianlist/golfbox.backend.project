/****** Object:  StoredProcedure [dbo].[SSP.Redmine.Issues.Save]    Script Date: 21-02-2016 23:12:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SSP.Redmine.Issues.Save]
	@Issue_ID int,
	@Issue_Parent_ID int = null,
	@Issue_Subject nvarchar(max),
	@Issue_Description nvarchar(max) = null,
	@Issue_EstimatedHours decimal(6,2) = null,
	@Issue_EstimatedRisk int = null,
	@Issue_DoneRatio int,
	@Status_ID int,
	@Project_ID int,
	@Tracker_ID int,
	@Priority_ID int,
	@Author_ID int,
	@AssignedTo_ID int = null,
	@FixedVersion_ID int,
	@Issue_DueDate datetime = null,
	@Issue_NextGroomingDate datetime = null,
	@Issue_CreatedOn datetime,
	@Issue_UpdatedOn datetime = null
	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	
IF EXISTS(
	SELECT 1 FROM [Redmine.Issues]
	WHERE Issue_ID = @Issue_ID
)
BEGIN
	UPDATE [Redmine.Issues]
	set		
		Issue_Parent_ID = @Issue_Parent_ID,
		Issue_Subject = @Issue_Subject,
		Issue_Description = @Issue_Description,
		Issue_EstimatedHours = @Issue_EstimatedHours,
		Issue_EstimatedRisk = @Issue_EstimatedRisk,
		Issue_DoneRatio = @Issue_DoneRatio,
		Status_ID = @Status_ID,
		Project_ID = @Project_ID,
		Tracker_ID = @Tracker_ID,
		Priority_ID = @Priority_ID,
		Author_ID = @Author_ID,
		AssignedTo_ID = @AssignedTo_ID,
		FixedVersion_ID = @FixedVersion_ID,
		Issue_DueDate = @Issue_DueDate,
		Issue_NextGroomingDate = @Issue_NextGroomingDate,
		Issue_CreatedOn = @Issue_CreatedOn,
		Issue_UpdatedOn = @Issue_UpdatedOn
	WHERE Issue_ID = @Issue_ID
END
ELSE
BEGIN
	INSERT INTO [Redmine.Issues]
	SELECT 
		@Issue_ID,
		@Issue_Parent_ID,
		@Issue_Subject,
		@Issue_Description,
		@Issue_EstimatedHours,
		@Issue_EstimatedRisk,
		@Issue_DoneRatio,
		@Status_ID,
		@Project_ID,
		@Tracker_ID,
		@Priority_ID,
		@Author_ID,
		@AssignedTo_ID,
		@FixedVersion_ID,
		@Issue_DueDate,
		@Issue_NextGroomingDate,
		@Issue_CreatedOn,
		@Issue_UpdatedOn
END		