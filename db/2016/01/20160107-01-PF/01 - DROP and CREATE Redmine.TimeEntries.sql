SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[Redmine.TimeEntries]
GO

CREATE TABLE [dbo].[Redmine.TimeEntries](
	[TimeEntry_ID] [int] NOT NULL,
	[TimeEntry_Hours] [decimal](6, 2) NOT NULL,
	[TimeEntry_SpentOn] [datetime] NOT NULL,
	[TimeEntry_Comment] [nvarchar](100) NULL,
	[Project_ID] [int] NOT NULL,
	[Issue_ID] [int] NOT NULL,
	[User_ID] [int] NOT NULL,
	[Activity_Id] [int] NOT NULL,
	[TimeEntry_CreatedOn] [datetime] NOT NULL,
	[TimeEntry_UpdatedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[TimeEntry_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



