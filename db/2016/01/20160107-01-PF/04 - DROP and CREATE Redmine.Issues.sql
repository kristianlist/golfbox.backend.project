/****** Object:  Table [dbo].[Redmine.Issues]    Script Date: 07-01-2016 11:24:21 ******/
DROP TABLE [dbo].[Redmine.Issues]
GO

/****** Object:  Table [dbo].[Redmine.Issues]    Script Date: 07-01-2016 11:24:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Redmine.Issues](
	[Issue_ID] [int] NOT NULL,
	[Issue_Parent_ID] [int] NULL,
	[Issue_Subject] [nvarchar](max) NOT NULL,
	[Issue_Description] [nvarchar](max) NULL,
	[Issue_EstimatedHours] [decimal](6,2) NULL,
	[Issue_EstimatedRisk] [int] NULL,
	[Issue_DoneRatio] [int] NOT NULL,
	[Status_ID] [int] NOT NULL,
	[Project_ID] [int] NOT NULL,
	[Tracker_ID] [int] NOT NULL,
	[Priority_ID] [int] NOT NULL,
	[Author_ID] [int] NOT NULL,
	[AssignedTo_ID] [int] NULL,
	[FixedVersion_ID] [int] NOT NULL,
	[Issue_NextGroomingDate] [datetime] NULL,
	[Issue_CreatedOn] [datetime] NOT NULL,
	[Issue_UpdatedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Issue_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


