/****** Object:  StoredProcedure [dbo].[SSP.Redmine.TimeEntries.Update]    Script Date: 06-01-2016 17:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SSP.Redmine.TimeEntries.Update]
	@TimeEntry_ID int,
	@TimeEntry_Hours [decimal](6, 2),
	@TimeEntry_SpentOn datetime,
	@TimeEntry_Comment nvarchar(100) = NULL,
	@Project_ID int,
	@Issue_ID int,
	@User_ID int,
	@Activity_Id int,
	@TimeEntry_CreatedOn datetime,
	@TimeEntry_UpdatedOn datetime = NULL
	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	
	UPDATE [Redmine.TimeEntries]
	SET 	
	TimeEntry_Hours = @TimeEntry_Hours,
	TimeEntry_SpentOn = @TimeEntry_SpentOn,
	TimeEntry_Comment = @TimeEntry_Comment,
	Project_ID = @Project_ID,
	Issue_ID = @Issue_ID,
	[User_ID] = @User_ID,
	Activity_Id = @Activity_Id,
	TimeEntry_CreatedOn = @TimeEntry_CreatedOn,
	TimeEntry_UpdatedOn = @TimeEntry_UpdatedOn
	WHERE TimeEntry_ID  = @TimeEntry_ID 
		