/****** Object:  StoredProcedure [dbo].[SSP.Redmine.TimeEntries.Insert]    Script Date: 06-01-2016 17:42:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SSP.Redmine.TimeEntries.Insert]
	@TimeEntry_ID int,
	@TimeEntry_Hours [decimal](6, 2),
	@TimeEntry_SpentOn datetime,
	@TimeEntry_Comment nvarchar(100) = NULL,
	@Project_ID int,
	@Issue_ID int,
	@User_ID int,
	@Activity_Id int,
	@TimeEntry_CreatedOn datetime,
	@TimeEntry_UpdatedOn datetime = NULL
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [Redmine.TimeEntries]
	SELECT 
		@TimeEntry_ID,
		@TimeEntry_Hours,
		@TimeEntry_SpentOn,
		@TimeEntry_Comment,
		@Project_ID int,
		@Issue_ID int,
		@User_ID int,
		@Activity_Id int,
		@TimeEntry_CreatedOn datetime,
		@TimeEntry_UpdatedOn datetime

	
