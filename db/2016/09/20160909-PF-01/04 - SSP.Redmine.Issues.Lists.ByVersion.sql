/****** Object:  StoredProcedure [dbo].[SSP.Issues.Users.Lists.All]    Script Date: 06-09-2016 21:37:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SSP.Redmine.Issues.Lists.ByVersion]
(
	@Version_ID INT
)
AS

SET NOCOUNT ON;

BEGIN
	SELECT * 
	FROM [Redmine.Issues]
	WHERE FixedVersion_ID = @version_ID

END

