SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE procedure [dbo].[SSP.Redmine.TimeEntries.Lists.ByUserList] (
	@UserIDList VARCHAR(MAX)
)

AS

SET NOCOUNT ON;

BEGIN
	SELECT e.* FROM [Redmine.TimeEntries] AS e
		INNER JOIN fn_ToIndexedArray_INT(@UserIDList) AS idList ON e.User_ID = idList.Value
	ORDER BY idList.[Index]
END






