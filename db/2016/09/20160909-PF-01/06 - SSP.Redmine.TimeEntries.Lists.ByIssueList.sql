

CREATE procedure [dbo].[SSP.Redmine.TimeEntries.Lists.ByIssueList] (
	@IssueIDList VARCHAR(MAX)
)

AS

SET NOCOUNT ON;

BEGIN
	SELECT e.* FROM [Redmine.TimeEntries] AS e
		INNER JOIN fn_ToIndexedArray_INT(@IssueIDList) AS idList ON e.Issue_ID = idList.Value
	ORDER BY idList.[Index]
END






