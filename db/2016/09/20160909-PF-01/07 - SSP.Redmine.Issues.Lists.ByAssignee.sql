/****** Object:  StoredProcedure [dbo].[SSP.Redmine.Issues.Lists.ByAssignee]    Script Date: 09-09-2016 13:08:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SSP.Redmine.Issues.Lists.ByAssignee]
(
	@UserIDList VARCHAR(MAX)
)
AS

SET NOCOUNT ON;

BEGIN
	SELECT e.* FROM [Redmine.Issues] e
		INNER JOIN fn_ToIndexedArray_INT(@UserIDList) AS idList ON e.AssignedTo_ID = idList.Value
	ORDER BY idList.[Index]

END

