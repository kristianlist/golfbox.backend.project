/****** Object:  UserDefinedFunction [dbo].[fn_ToIndexedArray_INT]    Script Date: 08-09-2016 12:39:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[fn_ToIndexedArray_INT]
(
	@ValueList VARCHAR(MAX)
)
RETURNS 
@ParsedList TABLE
(
	[Index] INT Identity(1,1),
	Value INT
)
AS
BEGIN
	DECLARE @Value VARCHAR(100), @Pos INT

	SET @ValueList = LTRIM(RTRIM(@ValueList))+ ','
	SET @Pos = CHARINDEX(',', @ValueList, 1)

	IF REPLACE(@ValueList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @Value = LTRIM(RTRIM(LEFT(@ValueList, @Pos - 1)))
			IF @Value <> ''
			BEGIN
				INSERT INTO @ParsedList (Value) 
				VALUES (CAST(@Value AS INT)) --Use Appropriate conversion
			END
			SET @ValueList = RIGHT(@ValueList, LEN(@ValueList) - @Pos)
			SET @Pos = CHARINDEX(',', @ValueList, 1)

		END
	END	
	RETURN
END

