SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SSP.Redmine.Projects.Update]
	@Project_ID int,
	@Project_Parent_ID int = null,
	@Project_Name nvarchar(100),
	@Project_Identifier nvarchar(100), 
	@Project_Description nvarchar(100) = null, 
	@Project_CreatedOn datetime,
	@Project_UpdatedOn datetime = null
	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	
	UPDATE [Redmine.Projects]
	SET 	 
	 Project_Parent_ID = @Project_Parent_ID,
	 Project_Name = @Project_Name,
	 Project_Identifier = @Project_Identifier, 
	 Project_Description = @Project_Description, 
	 Project_CreatedOn = @Project_CreatedOn,
	 Project_UpdatedOn = @Project_UpdatedOn
	WHERE Project_ID = @Project_ID
		