SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Redmine.TimeEntries](
	[TimeEntry_ID] [int] NOT NULL,
	[TimeEntry_Hours] [int] NOT NULL,
	[TimeEntry_SpentOn] [datetime] NOT NULL,
	[TimeEntry_Comment] [nvarchar](100) NULL,
	[Project_ID] [int] NOT NULL,
	[Issue_ID] [int] NOT NULL,
	[User_ID] [int] NOT NULL,
	[Activity_Id] [int] NOT NULL,
	[TimeEntry_CreatedOn] [datetime] NOT NULL,
	[TimeEntry_UpdatedOn] [datetime] NULL,
	PRIMARY KEY ([TimeEntry_ID])
) 

GO


