SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SSP.Redmine.Users.Insert]
	@User_ID int,	
	@User_Login nvarchar(100), 
	@User_Firstname nvarchar(100),
	@User_Lastname nvarchar(100),
	@User_Email nvarchar(100)
	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [Redmine.Users]
	SELECT 
	@User_ID,	
	@User_Login, 
	@User_Firstname,
	@User_Lastname,
	@User_Email

	
