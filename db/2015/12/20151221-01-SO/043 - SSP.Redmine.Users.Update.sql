SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SSP.Redmine.Users.Update]
	@User_ID int,	
	@User_Login nvarchar(100), 
	@User_Firstname nvarchar(100),
	@User_Lastname nvarchar(100),
	@User_Email nvarchar(100)
	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON
	
	
	UPDATE [Redmine.Users]
	SET 	
	User_Login = @User_Login, 
	User_Firstname = @User_Firstname,
	User_Lastname = @User_Lastname,
	User_Email = @User_Email
	WHERE [User_ID] = @User_ID
		