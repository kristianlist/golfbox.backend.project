SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SSP.Redmine.TimeEntries.Delete]
	@TimeEntry_ID int	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	DELETE FROM [Redmine.TimeEntries]
	WHERE TimeEntry_ID = @TimeEntry_ID

	
