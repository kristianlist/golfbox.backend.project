SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Redmine.Users](
	[User_ID] [int] NOT NULL,
	[User_Login] [nvarchar](100) NOT NULL,
	[User_Firstname] [nvarchar](100) NOT NULL,
	[User_Lastname] [nvarchar](100) NOT NULL,
	[User_Email] [nvarchar](100) NOT NULL,
	PRIMARY KEY ([User_ID])
) 

GO


