SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[SSP.Redmine.Users.Load] (
	@User_ID INT
)

AS

SET NOCOUNT ON;

BEGIN

	SELECT * FROM [Redmine.Users]
	WHERE [User_ID] = @User_ID;

END



