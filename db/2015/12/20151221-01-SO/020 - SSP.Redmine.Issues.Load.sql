SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[SSP.Redmine.Issues.Load] (
	@Issue_ID INT
)

AS

SET NOCOUNT ON;

BEGIN

	SELECT * FROM [Redmine.Issues]
	WHERE Issue_ID = @Issue_ID;

END



