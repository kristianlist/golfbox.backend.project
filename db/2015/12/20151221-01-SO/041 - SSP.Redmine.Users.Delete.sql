SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SSP.Redmine.Users.Delete]
	@User_ID int	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	DELETE FROM [Redmine.Users]
	WHERE [User_ID] = @User_ID

	
