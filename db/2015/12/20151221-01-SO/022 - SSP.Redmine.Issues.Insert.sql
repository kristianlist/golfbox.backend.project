SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SSP.Redmine.Issues.Insert]
	@Issue_ID int,
	@Issue_Parent_ID int = null,
	@Issue_Subject nvarchar(max),
	@Issue_Description nvarchar(max) = null,
	@Issue_DoneRatio int,
	@Project_ID int,
	@Tracker_ID int,
	@Priority_ID int,
	@Author_ID int,
	@AssignedTo_ID int = null,
	@FixedVersion_ID int,
	@Issue_CreatedOn datetime,
	@Issue_UpdatedOn datetime = null
	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	INSERT INTO [Redmine.Issues]
	SELECT 
		@Issue_ID,
		@Issue_Parent_ID,
		@Issue_Subject,
		@Issue_Description,
		@Issue_DoneRatio,
		@Project_ID,
		@Tracker_ID,
		@Priority_ID,
		@Author_ID,
		@AssignedTo_ID,
		@FixedVersion_ID,
		@Issue_CreatedOn,
		@Issue_UpdatedOn

	
