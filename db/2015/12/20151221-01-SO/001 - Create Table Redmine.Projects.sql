SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Redmine.Projects](
	[Project_ID] [int] NOT NULL,
	[Project_Parent_ID] [int] NULL,
	[Project_Name] [nvarchar](100) NOT NULL,
	[Project_Identifier] [nvarchar](100) NOT NULL,
	[Project_Description] [nvarchar](100) NULL,	
	[Project_CreatedOn] [datetime] NOT NULL,
	[Project_UpdatedOn] [datetime] NULL,
	PRIMARY KEY (Project_ID)
) 

GO


