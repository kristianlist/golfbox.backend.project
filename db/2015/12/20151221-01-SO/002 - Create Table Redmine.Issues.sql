SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Redmine.Issues](
	[Issue_ID] [int] NOT NULL,
	[Issue_Parent_ID] [int] NULL,
	[Issue_Subject] [nvarchar](max) NOT NULL,
	[Issue_Description] [nvarchar] (max) NULL,	
	[Issue_DoneRatio] [int] NOT NULL,	
	[Project_ID] [int] NOT NULL,
	[Tracker_ID] [int] NOT NULL,
	[Priority_ID] [int] NOT NULL,
	[Author_ID] [int] NOT NULL,
	[AssignedTo_ID] [int] NULL,
	[FixedVersion_ID] [int] NOT NULL,
	[Issue_CreatedOn] [datetime] NOT NULL,
	[Issue_UpdatedOn] [datetime] NULL,
	PRIMARY KEY (Issue_ID)
) 

GO


