SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[SSP.Redmine.Projects.Load] (
	@Project_ID INT
)

AS

SET NOCOUNT ON;

BEGIN

	SELECT * FROM [Redmine.Projects]
	WHERE Project_ID = @Project_ID;

END



