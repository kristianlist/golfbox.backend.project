SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[SSP.Redmine.TimeEntries.Load] (
	@TimeEntry_ID INT
)

AS

SET NOCOUNT ON;

BEGIN

	SELECT * FROM [Redmine.TimeEntries]
	WHERE TimeEntry_ID = @TimeEntry_ID;

END



