SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SSP.Redmine.Projects.Delete]
	@Project_ID int	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	DELETE FROM [Redmine.Projects]
	WHERE Project_ID = @Project_ID

	
