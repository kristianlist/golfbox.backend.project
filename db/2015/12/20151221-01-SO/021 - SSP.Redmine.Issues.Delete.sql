SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[SSP.Redmine.Issues.Delete]
	@Issue_ID int	
AS
	SET NOCOUNT ON
	SET XACT_ABORT ON

	DELETE FROM [Redmine.Issues]
	WHERE Issue_ID = @Issue_ID

	
